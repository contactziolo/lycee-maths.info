---
hide:
  -footer
---
#
**Devoirs pour les élèves de Terminale en spécialité NSI**

!!! attention
    - N'hésitez pas à bien chercher avant de regarder les corrigés.
    - La rédaction est importante.

## Récursivité

[2022-2023 1h](ds-recur-22-23.md) &nbsp;&nbsp; [2021-2022 1h](ds-recur-21-22.md)

## Programmation Orientée Objet

[2021-2022 1.25h](ds-poo-21-22.md)