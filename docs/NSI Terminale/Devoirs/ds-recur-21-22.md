---
show:
  - navigation
hide:
  - toc
---
=== "`Sujet`"
	#
	<center>

	**Devoir sur la récursivité**

	Durée : 55 min

	</center>

	## Exercice 1

	Représenter ce qui s’affiche sur la fenêtre graphique à l’exécution du code suivant :

	```python
	from turtle import *
	def dessine(n):
		if n>0:
			forward(n)
			right(90)
			dessine(n-5)
	dessine(100)
	```

	## Exercice 2

	Soit la fonction suivante :

	```python
	def pair(n) :
		while n>0 :
			n=n-2
		return n==0
	```

	Écrire une version récursive de cette fonction

	## Exercice 3

	Écrire une fonction récursive `inverse` qui prend en paramètre une chaîne de caractères ch et renvoie la chaîne obtenue en inversant l’ordre des caractères.
	Par exemple, `inverse('azerty')` a pour valeur la chaîne `'ytreza'`

	## Exercice 4

	On dispose d’une fonction `nettoie` qui prend en paramètre une liste triée et élimine les éléments identiques. Par exemple si la liste est `liste=[1,1,2,6,6,6,8,8,9,10]`, après l’instruction `nettoie(liste)`, cette liste a pour valeur `[1,2,6,8,9,10]`.

	```python
	def nettoie(L) :
		n=len(L)
		k=0
		while k<n-1 :
			if L[k]!=L[k+1] :
				k=k+1
			else :
				del L[k]
				n=len(L)
	```

	Écrire une fonction récursive de la fonction `nettoie`.

	## Exercice 5

	En géométrie euclidienne, un polygone régulier (convexe) est un polygone à la fois équilatéral (tous ses côtés ont la même longueur) et équiangle (tous ses angles ont la même mesure). Ci-dessous un polygone régulier à 9 côtés.

	<img src="../img/poly.png" width="40%" style="margin-left:15%;">

	La somme des angles d’un polygone régulier à $n$ côtés est $(n-2)\times 180$. Dans un carré cela fait $360$ ... et pour notre polygone à $9$ côtés cela fait $7\times 180$. 

	La mesure de l’un des angles ( ils sont tous égaux) est donné par la formule : $\frac{(n-2)\times 180}{n}$.

	On obtient $90$ pour un carré et $140$ pour notre polygone à $9$ cotés.

	Réaliser une fonction récursive, qui prend en paramètres le nombre de côtés($n$), la longueur d’un côté et l’angle de rotation et qui affiche un polygone régulier à $n$ côtés avec le module `turtle`.

=== "`Corrigé`"

	## Exercice 2

	Version récursive :

	```python
	def pair(n):
		if n<=1:
			return n==0
		else:
			return pair(n-2)
	```

	## Exercice 3

	```python
	def inverse(ch):
		if len(ch)==1:
			return ch
		else:
			return ch[-1]+inverse(ch[:-1])
	```

	## Exercice 4

	```python
	def nettoie(L):
		if len(L)==1:
			return L
		else:
			if L[0]==L[1]:
				return nettoie(L[1:])
			else:
				return [L[0]]+nettoie(L[1:])
	```

	## Exercice 5

	```python
	def pol(n,L):
		angle=180-((n-2)*180/n)
		poly(n,L,angle)
		
	def poly(n,L,angle):
		if n==1:
			forward(L)
		else:
			forward(L)
			left(angle)
			poly(n-1,L,angle)
	```
