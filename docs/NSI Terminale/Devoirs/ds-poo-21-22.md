---
show:
  - navigation
hide:
  - toc
---
=== "`Sujet`"
    <center>

    # Devoir NSI : Programmation Orientée Objet

    *Durée : 1h15*

    </center>

    ## Exercice 1

    Dans le plan rapporté à un repère orthonormé, un point `p` est défini par ses deux coordonnées $x$ et $y$ qui sont des nombres réels.

    On définit la distance entre ce point et l’origine du repère par $\sqrt{x^2+y^2}$ .

    1. Définir la classe `Point` avec trois méthodes :

        + le constructeur qui prend par défaut les coordonnées `(0;0)`
        + une méthode `distance_a_zero`, qui est bien nommée
        + Une méthode `__repr__` qui permet un affichage correct sous la forme `(x;y)`.
            
    2. Si nous écrivons `p2=Point(4,3)` et `p3=Point(4,3)`, l’expression `p2==p3` retourne la valeur `False`. 

        Modifier ce comportement pour obtenir que deux points sont égaux si ils ont les mêmes coordonnées.

    3. Ajouter une méthode `distAB` pour obtenir la distance entre deux points.

    ## Exercice 2

    Le domino est un jeu très ancien constitué de 28 pièces toutes différentes. Sur chacune de ces pièces, il y a deux côtés qui sont constitués de $0$ (le côté reste blanc) à $6$ points noirs. Lorsque les deux côtés portent le même nombre de points, on l’appelle domino double. Lorsqu’une des faces est blanche, on l’appelle domino blanc.

    <center>
    <img src="../img/domi1.png" width="40%">
    </center>

    1. Proposer une classe `Domino` permettant de représenter une pièce. Les objets seront initialisés avec les valeurs des deux côtés (gauche et droite). 

        On définira des méthodes pour tester si le domino est double ou blanc. On implémentera également une méthode pour compter le nombre de points sur un domino.

        On ajoutera également une méthode qui affiche les valeurs des deux faces de manière horizontale pour un domino classique et de manière verticale pour un domino double, comme le montre la figure ci-dessous.

        <center>
        ```
        ---------
        | 4 | 1 |
        ---------

        |---|
        | 4 |
        |---|
        | 4 |
        |---|
        ```
        </center>

    2. Proposer une classe `JeuDeDomino` permettant de manipuler le jeu de domino complet. On créera une méthode pour mélanger le jeu et pour distribuer selon 2 joueurs ou plus.

        À 2 joueurs chacun prend 7 dominos, à 3 et 4 joueurs chacun prend 6 dominos, à 5 et 6 joueurs chacun prend 4 dominos.

    3. En utilisant cette classe, on affichera le jeu de 2 joueurs, ainsi que le jeu restant (la pioche). Pour chaque joueur, on affichera le nombre total de points dans son jeu.

=== "`Corrigé`"

    ## Exercice 1

    ```python
    from math import sqrt

    class Point:
        def __init__(self,x=0,y=0):
            self.x=x
            self.y=y
            
        def distance_a_zero(self):
            return sqrt(self.x**2+self.y**2)
        
        def __eq__(self,other):
            return self.x==other.x and self.y==other.y
        
        def __repr__(self):
            return '('+str(self.x)+';'+str(self.y)+')'
        
        def distAB(self,other):
            return sqrt((self.x-other.x)**2+(self.y-other.y)**2)
    ```
    ## Exercice 2
    
    ```python
    from random import *
    class Domino:
        def __init__(self,faceA,faceB):
            if faceA>6 or faceB>6 or faceA<0 or faceB<0:
                raise Exception('ah mais non, ça va pas du tout')
            self.faceA=faceA
            self.faceB=faceB

        def double(self):
            return self.faceA==self.faceB

        def __repr__(self):
            if self.double():
                return('-\n'+str(self.faceA)+'\n-\n'+str(self.faceB)+'\n-')
            else:
                return('|'+str(self.faceA)+'|'+str(self.faceB)+'|')
        
        def total(self):
            return self.faceA+self.faceB
        
        def blanc(self):
            return self.faceA==0 and self.faceB==0

    class JeuDeDomino:
        def __init__(self,nbjoueurs):
            self.nbjoueurs=nbjoueurs
            self.pioche=[Domino(i,j) for i in range(0,7) for j in range(i,7)]
            self.jeux=[]
        
        def distribuer(self):
            nbcartes=0
            shuffle(self.pioche)
            if self.nbjoueurs==2:
                nbcartes=7
            elif self.nbjoueurs==3 or nbjoueurs==4:
                nbcartes=6
            elif self.nbjoueurs==5 or nbjoureurs==6:
                nbcartes=4
            for i in range(self.nbjoueurs):
                a=[]
                for i in range(nbcartes):
                    a.append(self.pioche.pop())
                self.jeux.append(a)
                
        def __repr__(self):
            chaine='nbrjoueurs = '+str(self.nbjoueurs)
            chaine=chaine+'\nPIOCHE\n'
            for elt in self.pioche:
                chaine=chaine+'\n'+str(elt)
            for i in range(self.nbjoueurs):
                chaine=chaine+'\nJOUEUR'+str(i+1)+'\n'
                for elt in self.jeux[i]:
                    chaine=chaine+'\n'+str(elt)
            
            return chaine
    ```