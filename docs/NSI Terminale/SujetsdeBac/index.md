---
hide:
  -footer
---
#
**Devoirs pour les élèves de Terminale en spécialité NSI**

!!! attention
    - N'hésitez pas à bien chercher avant de regarder les corrigés.
    - La rédaction est importante.

## 2022

[Centres étrangers J1](data\2022-centres-etranger-1.pdf)