---
hide:
  - navigation
  - toc
  - footer
---
#
Bonjour, soyez les bienvenus sur ce site.

On propose ici des cours, des exercices, des devoirs et activités de niveau lycée en spécialité mathématiques et en spécialité Numérique et Sciences Informatiques.

Les documents proposés ici complètent les cours donnés en présentiel.

Les devoirs sont des propositions personnelles ou des réécritures de problèmes classiques qui ont été proposées au cours des 10 dernières années.

Certains exercices sont d'un niveau un peu difficile: ils prépareront à merveille les élèves qui souhaitent réussir dans des filières exigeantes, mais un élève peut tout à fait réussir son année, obtenir son bac et, éventuellement, profiter plus tard d'une vie confortable sans les traiter !


Tous les fichiers du site peuvent être consultés, téléchargés et modifiés librement pour un usage personnel ou en classe.

Bon travail