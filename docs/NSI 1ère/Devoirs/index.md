---
hide:
  -footer
---
#
**Devoirs pour les élèves de Première en spécialité NSI**

## Bases de Python

[2022-2023 G1 1h](ds-bases-22-23AA.md) &nbsp;&nbsp; [2022-2023 G2 1h](ds-bases-22-23BA.md) &nbsp;&nbsp;