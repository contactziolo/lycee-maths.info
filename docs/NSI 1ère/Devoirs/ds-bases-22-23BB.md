---
date: September 22, 2022
geometry: "left=2cm,right=2cm,top=1cm,bottom=1.5cm"
output: pdf_document
show:
  - navigation
hide:
  - toc
---
<center>
# **Devoir sur les bases de Python**

*L'usage de la calculatrice n'est pas autorisé*

*Durée: 55 min*
</center>

## Exercice 1
Écrire ci-dessous les instructions afin que la variable `score`  soit multipliée par 20:

## Exercice 2
Que va afficher le script python suivant ?
```python
a = 10 
if a < 5:
    a = 20 
elif a > 1:
    a = 500 
elif a > 100:
    a= 1 
else:
    a= 0 
print(a)
```
|**a)**|**b)**|**c)**|**d)**|
|--------|--------|--------|--------|
|    1    |    0    |    500    |    20    |

## Exercice 3

Lequel des programmes ci-dessous affichera 1, 2, 3, 4, 5, 6, 7, 8 ?

a)
```python
a= 1
while a < 9: 
    a = a+ 1
    print(a)
```
b)  
```python
a= 1
while a <= 8:
    print(a)
    a = a+ 1
```
c) 
```python
for i in range (9) :
    print(i)
``` 
d)  
```python
for i in range (8) :
    print(i)
```

## Exercice 4
Quel est le résultat attendu après l’exécution de ce programme ?
```python
a= 5
b = 10
if a > 5 and b == 10 :
    print("toto") 
else :
    print("titi")
if a > 5 or b == 10 :
    print("tata") 
else :
    print("tutu")
```
|**a)**|**b)**|**c)**|**d)**|
|--------|--------|--------|--------|
|    toto    |    toto    |    titi    |    titi    |
|    tata    |    tutu    |    tata    |    tutu    |

## Exercice 5
Quelle est la réponse affichée après l’exécution du programme Python suivant ?
```python
u=5
for i in range(3):
    u=2*u-1
print(u)
```

|**a)**|**b)**|**c)**|**d)**|
|--------|--------|--------|--------|
|    compteur var    |    11 100    |    10 90    |    Rien ne s’affiche, on est dans une boucle infinie.    |

## Exercice 6
Que va afficher le programme suivant ?
```python
i=5
i += 1
if i % 3 == 0:
    print(i, "est divisible par 3") 
else:
    print(i, "n’est pas divisible par 3")
```
|**a)**|**b)**|**c)**|**d)**|
|--------|--------|--------|--------|
|    i est divisible par 3    |    i n'est pas divisible par 3    |    5 n’est pas divisible par 3    |    6 est divisible par 3    |

## Exercice 7
On considère le programme suivant :
```python
    for lettre in "LUNDI":
    print(lettre)
```
 Écrire ci-contre ce qui s'affiche en console lors de l'exécution de ce programme.

## Exercice 8
Proposer un code où :

- l'utilisateur doit rentrer un texte au clavier.
- le programme remplace alors toutes les voyelles par des `e`.

## Exercice 9
Écrivez une fonction `melanger` qui mélange aléatoirement les éléments d'une liste (dans une nouvelle liste).
Des fonctions Python qui font ce genre de choses, telles que `shuffle` ne sont pas autorisées ici.
Par exemple, si L vaut [0, 1, 2, 3] `melanger(L)` pourrait renvoyer `[3, 1, 0, 2]`.