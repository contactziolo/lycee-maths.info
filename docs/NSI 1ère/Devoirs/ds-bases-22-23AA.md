---
date: September 22, 2022
geometry: "left=2cm,right=2cm,top=1cm,bottom=1.5cm"
output: pdf_document
show:
  - navigation
hide:
  - toc
---
<center>
# **Devoir sur les bases de Python**

*L'usage de la calculatrice n'est pas autorisé*

*Durée: 55 min*

</center>
## Exercice 1
Écrire ci-dessous les instructions afin que la variable `points_de_vie` soit diminuée de 3:


## Exercice 2
Que va afficher le script python suivant ?
```python
a = 20 
if a < 5:
    a = 10 
elif a > 1:
    a = 1 
elif a > 100:
    a= 500 
else:
    a= 0 
print(a)
```

|**a)**|**b)**|**c)**|**d)**|
|--------|--------|--------|--------|
|    0    |    1    |    20    |    500    |

## Exercice 3

Lequel des programmes ci-dessous affichera 10, 11, 12, 13, 14, 15, 16, 17 ?

a)  
```python
a= 9
while a <= 17:
    print(a)
    a = a+ 1
```
b)  
```python
a= 10
while a < 17: 
    a = a+ 1
    print(a)
```
c)  
```python
for i in range (10,17) :
    print(i)
```
d)  
```python
for i in range (8) :
    print(i+10)
```

## Exercice 4
Quel est le résultat attendu après l’exécution de ce programme ?
```python
a= 5
b = 10
if a > 5 and b == 10 :
    print("toto") 
else :
    print("titi")
if a > 5 or b == 10 :
    print("tata") 
else :
    print("tutu")
```

|**a)**|**b)**|**c)**|**d)**|
|--------|--------|--------|--------|
|    titi    |    titi    |    toto    |    toto    |
|    tata    |    tutu    |    tata    |    tutu    |

## Exercice 5
Quelle est la réponse affichée après l’exécution du programme Python suivant ?
```python
var = 0
compteur = 0
while var < 100 :
    var = 10*compteur 
    compteur += 1
print(compteur,var)
```

|**a)**|**b)**|**c)**|**d)**|
|--------|--------|--------|--------|
|    Rien ne s’affiche, on est dans une boucle infinie.    |    10 90    |    11 100    |    compteur var    |

## Exercice 6
Que va afficher le programme suivant ?
```python
i=5
i += 1
if i % 3 == 0:
    print(i, "est divisible par 3") 
else:
    print(i, "n’est pas divisible par 3")
```

|**a)**|**b)**|**c)**|**d)**|
|--------|--------|--------|--------|
|    5 n’est pas divisible par 3    |    6 est divisible par 3    |    i est divisible par 3    |    i n'est pas divisible par 3    |

## Exercice 7
On considère le programme suivant :
```python
    for lettre in "MARDI":
        print(lettre)
```
 Écrire ci-contre ce qui s'affiche sur la console lors de l'exécution de ce programme.

## Exercice 8

Une liste de notes et de coefficients est donnée sous la forme: `L=[note1,coef1,note2,coef2,...]`
Proposer un code pour que le programme affiche alors la moyenne des notes ainsi que la plus grande note de la liste.

## Exercice 9
Le but de cet exercice est d'écrire un programme qui joue à papier-caillou-ciseaux.
Écrire un programme qui prend les choix des deux joueurs en paramètres et qui retourne 1 si le premier joueur gagne, -1 si c'est le second joueur, et 0 sinon.
Modifier le programme pour que l'on puisse jouer 50 fois à ce jeu. Les choix des joueurs sont aléatoires.
On affichera à la fin quel est le joueur gagnant.

