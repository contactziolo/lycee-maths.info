---
hide:
  - footer
  - toc
---
#
L'année de première dans la spécialité Maths est une année délicate pour les élèves, on consolide les bases de calcul et on commence à apprendre une certaine rigueur dans les écritures.

Pour réussir en première, il est conseillé :

  - de travailler régulièrement

  - de commencer à réfléchir sur les attentes de la rédaction.

  - de savoir déterminer les limites des outils qui sont présentés: c'est très bien de savoir quand un outil doit être utilisé, mais il est encore plus important de savoir la liste des outils dont on dispose pour résoudre un problème, et de savoir quand on ne doit pas les utiliser !

**L'objectif principal de l'année de première est d'acquérir des méthodes et de commencer à se préparer du mieux possible pour le baccalauréat qui aura lieu pendant l'année de terminale.**

Pour cela, vous disposez des devoirs, avec leurs corrigés, proposés ces dernières années en spécialité mathématiques.
Essayez bien sûr de chercher par vous-même avant de regarder le corrigé.

Si vous pensez avoir correctement répondu aux questions, veillez à bien vérifier que votre rédaction est pertinente et précise.

Il peut bien sûr y voir des erreurs, dans les sujets ainsi que dans les corrigés, n'hésitez pas à le signaler.