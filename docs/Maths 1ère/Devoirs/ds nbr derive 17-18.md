---
show:
    - navigation
hide:
    - toc
---

<center>

# *Devoir Nombre dérivé*

*L'usage de la calculatrice, contrairement à celui de la fonction dérivée, est autorisé.*

</center>

## Exercice 1

On considère la fonction $f$ définie sur $\mathbb{R}$ par $f(x)=x^2-5x+2$. 

On appelle $\text{C}_f$ sa courbe représentative. 

1. **a)** Montrer que le taux de variation de $f$ entre $3$ et $3+h$ vaut $T(h)=h+1$.

    **b)** En déduire la valeur du nombre dérivé $f'(3)$. 

2. Déterminer l’équation réduite de la tangente à la courbe $\text{C}_f$ au point $\text{A}$ d’abscisse $3$.

## Exercice 2

Soit $f(x)=(x-1)\sqrt{1-x^2}$.

1. Déterminer l'ensemble de définition de $f$.

2. Etudier la dérivabilité de $f$ en $-1$.

## Exercice 3
On donne ci-dessous un tracé de la courbe représentative $\text{C}_f$ d'une fonction $f$ définie sur $[-1;3]$.

<center>
    <img src="../img/courbe17-18.png" width="40%">
</center>

La droite $\text{T}$ tracée est la tangente à $\text{C}_f$ au point d’abscisse $1$. 

Aux points d’abscisses $0$ et $2$ les tangentes à $\text{C}_f$ sont parallèles à l’axe des abscisses. 

1. Déterminer, à l’aide du graphique, les valeurs de $f'(0)$, $f'(1)$ et $f'(2)$. 

2. En déduire les équations réduites des tangentes à $\text{C}_f$ aux points d’abscisses $0$ ; $1$ et $2$. 

## Exercice 4
Montrer que la tangente à la courbe $\text{C}$ d'équation $y=-x^4+2x^2+x$ au point $\text{A}(-1;0)$ est aussi tangente à $\text{C}$ en un autre point que l'on précisera.