---
show:
    - navigation
hide:
    - toc
---
#
=== "`Sujet`"

    <center>
    *L'usage de la calculatrice n'est pas autorisé.*

    *Durée: 40 minutes*

    **Consignes générales** : *Toutes les questions sont indépendantes et peuvent donc être traitées dans n’importe quel ordre. Le barème est indicatif. Toutes les réponses doivent être justifiées, sauf mention expresse du contraire. Dans ce dernier cas, une mauvaise réponse est pénalisante, afin de décourager les réponses « au hasard ».*

    </center>

    **Exercice n°1**

    Résoudre dans $\mathbb{R}$ les équations suivantes :

    1. $3x^2-4x+1=0$

    2. $(-3x+2)(2x-1)-(3x-2)(3x+4)=0$

    3. $3x^2+4x-4=0$

    4. $\frac 1 2x^2-\frac 4 5x+2=0$

    **Exercice n°2**

    On appelle $f$ la fonction définie par $f(x)=\mathit{ax}^2+\mathit{bx}+c$, $a\neq 0$ . 

    Pour chacune des affirmations suivantes, dire si elle est vraie ou fausse, en justifiant rapidement. 

    1. Si $a>0$ alors $f$ a un maximum.

    2. Si $a>0$ et $c<0$ alors l’équation $f(x)=0$ admet deux solutions.

    3. Si l’équation $f(x)=0$ admet deux solutions, alors $a$ et $c$ sont de signes contraires.

    4. Si $a>0$, $b>0$ et $c>0$  alors pour tout réel $x$, $f(x)>0$.

    **Exercice n°3**

    Soient les fonctions $f ,g ,h$  et $k$  définies sur $\mathbb{R}$ telles que $f(x)=-x^2-3x-3$, $g(x)=x^2+3x-3$, $h(x)=2x^2+6x-3$  et $k(x)=x^2-x-3$ .

    Colorier proprement $C_f$ en rouge, $C_g$  en vert, $C_h$ en bleu et $C_k$ en noir. 

    Justifier toutes vos réponses.

    <img src="../img/graph15-16.png" width="60%" style="margin-left:15%;">

=== "`Corrigé`"

    **Exercice 1**

    1. $1$ est racine évidente, et on a $x_1x_2=\frac 1 3$. 

        L'ensemble $\text S$ des solutions est donc:
        
        $$\text S=\left\{\frac 1 3\mathrm ;1\right\}$$

    2. $(-3x+2)(2x-1)-(3x-2)(3x+4)=(3x-2)(-2x+1-(3x+4))=(3x-2)(-5x-3)$
        
        L'ensemble $\text S$ des solutions est donc:

        $$\text S=\left\{-\frac 3 5\mathrm ;\frac 2 3\right\}$$

    3.  $3x^2+4x-4$ est un trinôme du second degré, calculons son discriminant $\Delta$.

        $\Delta =16+4\times 3\times 4=64 \Leftrightarrow  \Delta >0$ 

        L'équation $3x^2+4x-4=0$ admet donc deux solutions : $x_1=\frac{-4-8} 6=-2$ et $x_2=\frac{-4+8} 6=\frac 2 3$. 
        
        L'ensemble $\text S$ des solutions de cette équation est donc :

        $$\text S=\left\{-2\mathrm ;\frac 2 3\right\}$$

    4.  $\frac 1 2x^2-\frac 4 5x+2$ est un trinôme du second degré, calculons son discriminant $\Delta$:

        $\Delta =\frac{16}{25}-4=-\frac{84}{25} \Leftrightarrow \Delta <0$

        $\frac 1 2x^2-\frac 4 5x+2=0$ n'admet pas de solutions:

        $$\text S=\emptyset$$



    **Exercice 2**

    1. $a>0 \Rightarrow$ la parabole d'équation $y=\mathit{ax}^2+\mathit{bx}+c$ admet un minimum $\Rightarrow$ FAUX.

    2. $a>0$ et  $c<0 \Rightarrow \mathit{ac}<0 \Rightarrow -\mathit{ac}>0 \Rightarrow b^2-4\mathit{ac}>0 \Rightarrow \Delta>0 \Rightarrow f(x)=0$ admet deux solutions. VRAI.

    3. FAUX. $f(x)=0,25x^2+x+0,1$ admet deux solutions.

    4. FAUX. $f(x)=0,25x^2+x+0,1$ admet deux solutions donc change de signe.


    **Exercice 3**

    $C_f$ est la seule courbe avec les branches vers le bas.

    En $-1$ : $g(-1)=-5$ $h(-1)=-7$ et $k(-1)=-1$  permet de différencier les trois courbes restantes.
