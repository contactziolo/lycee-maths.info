---
show:
    - navigation
hide:
    - toc
---

<center>
# Devoir Dérivation

*L’usage de la calculatrice est autorisé*

*Un soin particulier sera apporté à la rédaction et aux justifications.*

*Durée : 55 min*
</center>

## Exercice 1 (2,5 points)
$\text{C}_f$ est la fonction définie sur $\mathbb{R}^*$ par $f(x)=x-\dfrac{1}{x}$.

1. Vérifiez que pour tout $h \ne 0$ et $1+h>0$ :

    $$f(1+h)=\dfrac{2h+h^2}{1+h}$$

2. Déduisez-en que $f$ est dérivable en $1$ et calculez $f'(1)$.

## Exercice 2 (2,5 points)
$\text{C}_f$ est la courbe représentative d’une fonction $f$ définie et dérivable sur $\mathbb{R}$. 

On donne:

$f(-3)=-1$ &nbsp;&nbsp;&nbsp;&nbsp; $f(-0)=-2$ &nbsp;&nbsp;&nbsp;&nbsp; $f(3)=0$ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $f(6)=4$

$f'(-3)=0$ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $f'(0)=-1$ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $f'(3)=3$ &nbsp;&nbsp;&nbsp;&nbsp; $f'(6)=0.5$

1. Placez les points $\text{A}$, $\text{B}$, $\text{C}$ et $\text{D}$ d’abscisses respectives $-3$; $0$; $3$ et $6$.
2. Construisez les tangentes à $\text{C}_f$ aux points $\text{A}$, $\text{B}$, $\text{C}$ et $\text{D}$.
3. Dessinez une allure possible de la courbe sur l’intervalle $[-3;6]$.

## Exercice 3 (4,5 points)
$f$ est une fonction et $a$ un nombre donné. $f$ est dérivable en $a$. Déterminez une équation de la tangente à la courbe représentative de $f$ au point d’abscisse $a$.

1. $f(x)=3x^3+5x-2$ et $a=-2$.

2. $f(x)=(2x+1)^{10}$ et $a=0$.

3. $f(x)=(4x+8)sqrt{x}$ et $a=4$.


## Exercice 4 (2,5 points)
$f$ est la fonction définie sur $\mathbb{R}-\{-1\}$ par $f(x)=\dfrac{2x}{x+1}$, et $\text{C}_f$ est sa courbe représentative.

1. Calculez $f'(x)$.

2. Quels sont les points de $\text{C}_f$ en lesquels la tangente à $\text{C}_f$ est parallèle à la droite d’équation $y=4x$?


## Exercice 5 (3 points)
La figure ci-dessous représente un écran de jeu vidéo. Un avion remonte l’écran de gauche à droite en suivant la courbe d’équation . L’avion peut tirer des missiles selon la tangente à sa trajectoire.

<center>
<img src="../img/avion.png" width="40%">
</center>

En quels points de sa trajectoire l’avion doit-il tirer ses missiles pour abattre successivement les quatre monstres situés en haut de l’écran en $(1;0)$, $(2;0)$, $(3;0)$ et $(4;0)$ ?