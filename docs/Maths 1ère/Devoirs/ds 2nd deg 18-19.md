---
show:
    - navigation
hide:
    - toc
---
#
=== "`Sujet`"
    <center>

    *La qualité de la rédaction sera prise en compte dans la notation.*

    *L'usage de la calculatrice est interdit pour ce devoir.*

    *Durée: 55 min*

    </center>

    **Exercice 1 (5 points)**

    1.  **a.** Résoudre l'équation $-6x^2+x+1=0$ .
        
        **b.** Résoudre $-x=1+\frac 1 x$ .

    2.  **a.** Déterminer le tableau de variations de la fonction $g$ définie sur ℝ par $g(x)=-x^2+x+1$.

        **b.** En déduire la résolution de l'inéquation $g(x)\geqslant \sqrt 5$ .

    3. Résoudre sur ℝ l'équation suivante : $\sqrt{-x+2}=2x-7$.

    **Exercice 2 (2 points)**

    Une porte est constituée :

    - d'un battant rectangulaire de hauteur notée $y$, de largeur notée $x$.

    - d'un cintre semi-circulaire.

    La hauteur totale de la porte est $h = 3 \text{m}$ et l'aire du battant rectangulaire est égale à 2 $\text{m}^2$.

    Quelle est la largeur de la porte ?

    <img src="../img/porte.png" width="40%" left="50" style="margin-left:15%;">

    **Exercice 3 (3 points)**

    On considère deux fonctions trinômes, $f$ et $g$, dont vous avez les représentations graphiques.

    1. Déterminer l'expression développée de $f(x)$.

        <img src="../img/foncf.png" width="30%" left="50" style="margin-left:15%;">

    2. Déterminer l'expression développée de $g(x)$.

        <img src="../img/foncg.png" width="30%" left="50" style="margin-left:15%;">

    **Exercice 4 (3 points)**

    1. Soit $2x^2+x-1$, trouver une racine évidente $x_1$ de ce trinôme.

        En déduire la seconde $x_2$.

    2. Résoudre l'inéquation suivante : $\frac{2x^2-x+1}{2x^2+x-1}\leqslant 0$ .

=== "`Corrigé`"

    **Exercice 1**

    1.  **a.** On a une équation du 2nd degré, calculons son discriminant $\Delta$ :

        $\Delta=b^2-4\mathit{ac}=1+4\times 6=25=5^2$. $\Delta>0$ donc deux racines distinctes.

        $x_1=\frac{-b-\sqrt{\Delta}}{2a}$ et $x_2=\frac{-b-\sqrt{\Delta}}{2a}$ .

        D’où $x_1=\frac{-6}{-12}=\frac 1 2$ et $x_2=\frac 4{-12}=-\frac{1} 3$ d’où l’ensemble $S$ des solutions de l’équation: $\text S=\left\{-\frac 1 3\mathrm ;\frac 1 2\right\}$

        **b.** On a une valeur interdite $x=0$ , donc on se place sur $\mathbb{R}^*$.

        $-x=1+\frac 1 x \Rightarrow -x^2=x+1 \Rightarrow x^2+x+1=0$

        C'est une équation du 2nd degré, calculons son discriminant $\Delta$ :

        $\Delta=b^2-4\mathit{ac}=1-4=-3$ donc $\Delta<0$. 

        Dans $\mathbb{R}$, il n'y a pas de solutions à l’équation de départ. $\text S={\emptyset}$ .

    2.  **a.** On a $a<0$ et le sommet de la parabole qui est $\left(\frac 1 2\mathrm ;\frac 5 4\right)$.

        On a donc le tableau des variations de $g$ :

        <img src="../img/varg.png" width="25%" style="margin-left:25%;">

        **b.** D’après le tableau ci-dessus, on a $g(x)\leqslant \frac 5 4$ donc $g(x)<\sqrt 5$ . L’inéquation $g(x)\geqslant \sqrt 5$ n’admet donc pas de solutions. L’ensemble $\text S$ des solutions est $\text S={\emptyset}$.

    3. On détermine l’ensemble de recherche des solutions : $-x+2\geqslant 0$ et $2x-7\geqslant 0$ d’où $x\leqslant 2$ et $x\geqslant \frac 7 2$ . 
    
        L’intersection de ces deux ensembles étant vide, il ne peut pas exister de solution à l’équation de départ. L’ensemble $\text S$ des solutions est $\text S={\emptyset}$ .

    **Exercice 2**

    x est la largeur de la porte. La hauteur du battant rectangulaire est $y=h-\frac x 2$ car le cintre a pour hauteur $\frac x 2$.

    L'aire du battant rectangulaire est donc : $x\left(3-\frac x 2\right)$ .

    On résout l'équation $x\left(3-\frac x 2\right)=2 \Leftrightarrow x^2–6x+4=0$

    C'est une équation du second degré, on calcule son discriminant $\Delta$:

    $\Delta = ( – 6 )^2 – 4 \times 1 \times 4 = 36 – 16 = 20 \Rightarrow \sqrt{\Delta }=\sqrt{20}=2\sqrt 5$

    D'où 2 solutions pour x : $x_1 = 3-\sqrt 5$ et $x_2 = 3+\sqrt 5$.

    **Exercice 3**

    1.  $f(x)=a(x-x_1)(x-x_2)=a(x+2)(x-5)$ 
        Avec le point $\text A(0\mathrm ;5)$ de $\text C_f$, on a $f(0)=5$ soit $-10a=5$ d’où $a=-\frac 1 2$.
        Donc $f(x)=-\frac 1 2(x+2)(x-5)=-\frac 1 2(x^2-3x-10) \Rightarrow f(x)=-\frac 1 2x^2+\frac 3 2x+5$.

    2.  $g(x)=a(x-x_{\text S})^2+y_{\text S}=a\left(x-\frac 5 2\right)^2+\frac{11} 2$. Avec le point $\text B(1\mathrm ;1)$ de $\text C_g$ , on a : $f(1)=1$

        D’où $a\left(1-\frac 5 2\right)^2+\frac{11} 2=1 \Rightarrow a\left(\frac 3 2\right)^2+\frac{11} 2=1 \Rightarrow \frac 9 4a=-\frac 9 2 \Rightarrow a=-2$ .

        Et $g(x)=-2\left(x-\frac 5 2\right)^2+\frac{11} 2=-2\left(x^2-5x+\frac{25} 4\right)+\frac{11} 2 \Rightarrow g(x)=-2x^2+10x-7$ .

    **Exercice 4** 

    1.  On teste $x_1=-1$ : $2(-1)^2-1-1=2-2=0$ donc $x_1=-1$ est une racine évidente.

        On a $x_1x_2=\frac c a$ soit $-x_2=\frac{-1} 2$ et $x_2=\frac 1 2$ .

    2. D’après 1., il y a deux valeurs interdites, $-1$ et $\frac 1 2$.

        On étudie $2x^2-x+1$, c’est un trinôme du 2nd degré, calculons son discriminant $\Delta=b^2-4\mathit{ac}=1-4\times 2=-7$ , on a $2>0$ donc $2x^2-x+1>0$.

        En revanche, $2x^2+x-1$ ayant deux racines avec $a>0$, on obtient le tableau de signe suivant :

        <img src="../img/sign.png" width="25%" style="margin-left:25%;">

        L’ensemble $\text S$ des solutions de l’inéquation est donc $\left]-1\mathrm ;\frac 1 2\right[$ .

