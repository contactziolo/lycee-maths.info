---
hide:
  -footer
---
#
**Devoirs pour les élèves de première en spécialité Maths**

!!! attention
    - N'hésitez pas à bien chercher avant de regarder les corrigés.
    - La rédaction est très importante.

## 2nd degré

[2020-2021](ds 2nd deg 20-21.md)    &nbsp;&nbsp; [2018-2019](ds 2nd deg 18-19.md)   &nbsp;&nbsp; [2017-2018](ds 2nd deg 17-18.md)   &nbsp;&nbsp; [2016-2017](ds 2nd deg 16-17.md)   &nbsp;&nbsp; [2015-2016](ds 2nd deg 15-16.md)

## Nombre dérivé

[2018-2019](ds nbr derive 18-19.md)    &nbsp;&nbsp;   [2017-2018](ds nbr derive 17-18.md)    &nbsp;&nbsp; [2016-2017](ds nbr derive 16-17.md)    &nbsp;&nbsp;

[2020-2021](ds nbr derive 20-21.md)  &nbsp;&nbsp; (deux premiers exercices)
