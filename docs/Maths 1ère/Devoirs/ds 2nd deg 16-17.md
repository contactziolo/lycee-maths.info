---
show:
    - navigation
hide:
    - toc
---
#
=== "`Sujet`"

    <center>

    *L'usage de la calculatrice n'est pas autorisé.*

    *Durée: 55 minutes*

    </center>

    **Exercice 1**

    Résoudre dans $\mathbb{R}$ chacune des équations ci-dessous :

    **a.** $x^2-x-2=0$     		

    **b.** $5x^3-3x^2-5x=0$		

    **c.** $\frac x{2x+3}=\frac{x-1}{3x}$

    **Exercice 2**

    Dans chacun des cas suivants. déterminer un trinôme $\text P$ du second degré tel que :

    1. $\text P$ admet pour racines les nombres $4$ et $7$.

    2. $\text P$ admet une racine double égale à $-5$. 

    3. $\text P$ admet pour racines les nombres $-9$ et $8$ et admet un maximum sur $\mathbb{R}$.

    4. $\text P$ n'admet aucune racine et admet un maximum sur $\mathbb{R}$.

    5. La courbe représentative de $\text P$ admet la droite d'équation $x=-5$ comme axe de symétrie et $\text P$ admet un minimum sur $\mathbb{R}$.

    **Exercice 3: Optimisation**

    $\text{AMN}$ est un triangle rectangle en $\text A$ tel que $\text{AM}=x \: \text{cm}$ et  $\text{AN}=5-x \: \text{cm}$. 

    1. Quelles sont les valeurs possibles pour $x$ ?

    2. Existe-t-il une position du point $\text M$ pour laquelle la quantité $\text{MN}^2$ est maximale ou minimale ? 

        Si tel est le cas, déterminer cette valeur et en déduire la plus grande ou la plus petite valeur de $\text{MN}$.

=== "`Corrigé`"

    **Exercice 1**

    **a.** $x^2-x-2=0$ admet pour solution évidente $x_1=-1$, ce trinôme du 2nd degré admet donc une seconde racine telle que: $x_1x_2=\frac c a$ d'où $x_1x_2=-2$ et $x_2=\frac{-2}{-1}=2$. 

    L'ensemble $\text S$ des solutions de cette équation est donc: 

    $$\text S=\{-1;2\}$$

    **b.** On a $5x^3-3x^2-5x=x(5x^2-3x-5)$ , $0$ fait donc partie des solutions de l'équation de départ.
        
    Résolvons $5x^2-3x-5=0$: 
        
    C'est une équation du second degré, déterminons son discriminant $\Delta$:

    $\Delta=9-4\times 5\times (-5) \Leftrightarrow \Delta=9+100 \Leftrightarrow \Delta=109$
        
    $\Delta>0$ donc l'équation du 2nd degré admet deux solutions: $x_1=\frac{3-\sqrt{109}}{10}$ et $x_2=\frac{3+\sqrt{109}}{10}$.

    Au final l'ensemble $\text S$ des solutions est: 

    $$\text S= \left\{\frac{3-\sqrt{109}}{10};0;\frac{3+\sqrt{109}}{10}\right\}$$

    **c.** $\frac x{2x+3}=\frac{x-1}{3x} \Leftrightarrow 3x^2=(2x+3)(x-1)$ avec  $x\neq 0$ et $x\neq -\frac 2 3$

    Dans la suite on considère donc que $x\neq 0$ et $x\neq -\frac 2 3$:

    $3x^2=2x^2-2x+3x-3 \Leftrightarrow x^2-x+3=0$

    Ceci est une équation du second degré, calculons son discriminant $\Delta$:

    $\Delta=1-4\times 3 \Leftrightarrow \Delta=-11$

    Le discriminant étant négatif l'ensemble $\text S$ des solutions est vide:

    $$\text S={\emptyset}$$

    **Exercice 2**

    1. $\text P(x)=(x-4)(x-7)$

    2. $\text P(x)=(x+5)^2$

    3. $\text P(x)=(-1)\times (x+9)(x-8)$

    4. $\text P(x)=-x^2-1$

    5. Les solutions sont symétriques donc $(x+10)x$ répond à la question.


    **Exercice 3**

    1. Les longueurs sont positives donc $x\geqslant 0$ et $5-x\geqslant 0$ d'où $x\in [0\mathrm ;5]$.

    2. On a $\text{MN}^2=\text{AM}^2+\text{AN}^2$ (Pythagore dans AMN rectangle en A).

        D'où: $\text{MN}^2=x^2+(5-x)^2 \Leftrightarrow \text{MN}^2=2x^2-10x+25$ 	
                
        $\text{MN}^2$ est une fonction du 2nd degré, déterminons ses variations sur $\left[0;5\right]$:

        On a $2>0$. $\text{MN}^2$ admet donc un minimum en $x_{\text S}=\frac{10} 4=2,5$ soit .

        On a alors $\text{MN}^2=2\times \left(\frac 5 2\right)^2-10\times \left(\frac 5 2\right)+25$. Soit $\text{MN}^2=\frac{25} 2$ .
            
        La plus petite valeur de $\text{MN}$ est donc $\frac 5{\sqrt 2}=\frac{5\sqrt 2} 2$.