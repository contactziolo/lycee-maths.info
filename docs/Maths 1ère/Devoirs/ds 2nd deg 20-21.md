---
show:
    - navigation
hide:
    - toc
---

=== "`Sujet`"

    <center>

    # **Devoir sur le Second degré**

    *L’usage de la calculatrice est autorisé.*

    *Un soin particulier sera apporté à la rédaction et à la justification.*

    *Le baréme est donné à titre indicatif.*

    *Durée : 1h*

    </center>

    **Exercice 1 (3,5 points)**

    Soit $f$ la fonction définie pour tout réel $x$ par $f(x)=-x^2+4x+12$.

    On note $C_f$ sa courbe représentative.

    1. a) Écrire $f(x)$ sous forme canonique.

        b) En déduire que $f$ admet un maximum que l’on précisera.

        En quelle valeur ce maximum est-il atteint?

    2. a) Factoriser $f(x)$ à partir de sa forme canonique.

        b) Résoudre l’équation $f(x)=0$ sans utiliser le discriminant.

    **Exercice 2 (2,5 points)**

    Résoudre les deux équations suivantes :

    $$(E_1) : 4(x+3)^2-(x-5)^2=0$$

    $$(E_2) : \frac{1}{x}+\frac{1}{x+15}=\frac{1}{56}$$

    **Exercice 3 (4 points)**

    Les deux graphiques ci-dessous correspondent chacun à la courbe représentative d'une fonction polynôme du second degré $f$.

    Dans chacun des cas, déterminer une expression de $f(x)$ .

    1. 

        <img src="../img/graph1.png" width="35%" style="margin-left:15%;">

    2. 

        <img src="../img/graph2.png" width="35%" style="margin-left:15%;">

    **Exercice 4 (2 points)**

    Quelles sont les dimensions d’un rectangle de périmètre 50 $\text{m}$ et d’aire 114 $\text{m}^2$ ?

    **Exercice 5 (4 points)**

    $m$ est un réel donné et $f$ la fonction trinôme définie par $f(x)=mx^{2}+{4}x+{2}(m-{1})$.

    1. a) Pour quelle(s) valeur(s) de $m$ l’équation $f(x)=0$ a-t-elle une seule solution ?

        b) Quel est l’ensemble des réels $m$ pour lesquels l’équation $f(x)=0$ a deux solutions distinctes ?

    2. Quel est l’ensemble des réels $m$ pour lesquels $f(x)<0$ pour tout réel $x$ ?

    3. Donner l’équation de la droite $(d)$ axe de symétrie pour la courbe représentative de $f$.

    **Exercice 6 Un problème chinois (bonus)**

    Une ville carrée de dimensions inconnues possède une porte au milieu de chacun de ses côtés.

    <center>
    ![](img/map.png)
    </center>

    Un arbre se trouve à 20 pas de la porte Nord, à l’extérieur de la ville. Il est visible d’un point que l’on atteint en faisant 14 pas vers le sud à partir de la porte Sud, puis 1775 pas vers l’ouest. Quelle est la dimension de chaque côté ?

=== "`Corrigé`"

    **Exercice 1**

    1.a)  $f(x)=-(x^2-4x-12)=-((x-2)^2-4-12)=-(x-2)^2+16$ 

    b) On a  $(x-2)^2\geqslant 0 \rightarrow -(x-2)^2 \leqslant 0 \Rightarrow -(x-2)^2+16 \leqslant 16 \Rightarrow f(x)\leqslant 16$ 

    De plus  $f(2)=16$ donc  $f$ admet bien 16 pour maximum, atteint pour $x=2$ .

    2. a)  $f(x)=-((x-2)^2-16)=-((x-2)^2-4^2)=-(x-2-4)(x-2+4)=-(x-6)(x+2)$.

    b) D'après l'écriture qui précède, on déduit que  $f(x)=0$ pour  $x$ ${\in}$\{-2;6\}.

    **Exercice 2**

    $4(x+3)^2-(x-5)^2=(2x+6)^2-(x-5)^2=(2x+6-(x-5))(2x+6+x-5)=(x+11)(3x+1)$

    D'où  $(\text E_1) \Leftrightarrow (x+11)(3x+1)=0 \Leftrightarrow x=-11$ ou $x=-\frac 1 3$

    L'ensemble  $\text S_1$ des solutions de  $(\text E_1)$ est donc  $\text S_1=\left\{-11\mathrm ;-\frac 1 3\right\}$.

    $\frac 1 x+\frac 1{x+15}=\frac 1{56} \Leftrightarrow \frac{2x+15}{x(x+15)}=\frac 1{56} \Leftrightarrow 56(2x+15)=x(x+15)$ avec  $x\notin \{0 \mathrm ;-15\}$ .

    Dans la suite, on suppose que  $x\notin \{0 ;-15\}$ :

    $56(2x+15)=x(x+15) \Leftrightarrow 112x+840=x^2+15x \Leftrightarrow x^2-97x-840=0$ 

    Cette dernière équation est du second degré, calculons le discriminant $\Delta$:

    $\Delta=97^2-4\times 1\times (-840)=12769=113^2$

    L'équation  $(\text E_2)$ admet donc deux solutions distinctes :

    $x_1=\frac{97-113} 2=-8$ et  $x_2=\frac{97+113} 2=105$.

    L'ensemble  $\text S_2$ des solutions de  $(\text E_2)$ est donc  $\text S_2=\left\{-8\mathrm ;105\right\}$.

    **Exercice 3**

    1.  $-2$ et  $1$ sont solutions de  $f(x)=0$ , donc  $f$ s'écrit  $f(x)=a(x+2)(x-1)$ .

        De plus,  $f(0)=-1 \Rightarrow f(0)=a\times 2\times (-1)=-1 \Rightarrow a=\frac 1 2$ .

        D'où  $f(x)=\frac 1 2(x+2)(x-1)$.

    2. Le sommet de la parabole est le point de coordonnées  $(-1\mathrm ;-1)$.

        D'où  $-\frac b{2a}=-1 \Rightarrow b=2a$ et $f(-1)=-1 \Rightarrow a-b+c=-1 \Rightarrow c=a-1$ 

        $f$ s'écrit donc  $f(x)=\mathit{ax}^2+2\mathit{ax}+a-1$ 

        De plus,  $f(1)=-2 \Rightarrow a+2a+a-1=-2 \Rightarrow a=-\frac 1 4$

        D'où  $f(x)=-\frac 1 4x^2-\frac 1 2x-\frac 5 4$.

    **Exercice 4**

    On pose  $a$ la longueur et  $b$ la largeur du rectangle (On suppose  $a>b$ ).

    On a donc 2a+2b=50 soit  $a+b=25$ et  $ab=114$ .

    On retrouve la somme et le produit des racines d'un trinome.

    $a$ et $b$ sont les solutions de l'équation :  $x^2-25x+114=0$ 

    Cette dernière équation est du second degré, calculons le discriminant $\Delta$:

    $\Delta=(-25)^2-4\times 114=169=13^2$

    L'équation admet donc deux solutions distinctes:

    $b=25-\frac{\sqrt{169}} 2=6$ et $a=25+\frac{\sqrt{169}} 2=19$ 

    **Exercice 5**

    1. a)  $\mathit{mx}^2+4x+2(m-1)=0$

        (E) est une équation du second degré, calculons son discriminant $\Delta$:

        $\Delta=4^2-8m(m-1)=-8m^2+8m+16=8(-m^2+m+2)$

        Si  $(\text E)$ admet une seule solution, alors  $\Delta=0$ . Déterminons donc les racines de  $-m^2+m+2$.

        On reconnaît -1 comme racine évidente, comme  $x_1x_2=-2$ , alors la deuxième racine est 2.

        L'équation (E) admet donc une seule solution si et seulement si  $m \in \{-1;2\}$ .

        b)  $g(m)=-m^2+m+2$ est une fonction polynomiale du second degré avec  $a=-1$ . $g$ est donc positive entre ses racines.

        L'équation (E) admet donc deux solutions si et seulement si  $m \in ]-1;2[$ .

    2. On a  $f(x)<0$ ou  $f(x)>0$ si $f$ n'admet aucune racine, donc si $m \in ]-\infty;-1[{\cup}]2
    ;+\infty [$ .

        De plus, pour avoir $f(x)<0$ , il faut que $f$ admette un maximum, donc pour  $m<0$ .

        On a donc  $f(x)<0$ pour $m \in ]-\infty;-1[$ .

    3. $x=-\frac b{2a}$ est axe de symétrie soit  $x=-\frac 2 m$.

    **Exercice 6 (bonus)**

    Soit  $N$ la porte nord et  $2x$ la dimension du côté de la ville.

    On a:

    - $\text N$ appartient à  $(\text{AB})$,
    - $\text D$ appartient à  $(\text{AC})$,
    - $(\text{DN})$ est parallèle à  $(\text{CB})$.

    D'après le théorème de Thalès, on a:

    $\frac{\text{DN}}{\text{CB}}=\frac{\text{AN}}{\text{AB}}$ .

    En remplaçant par les longueurs en fonction de  $x$ , on obtient:

    $\frac x{1775}=\frac{20}{2x+34}$ .

    Pour  $x>0$ , on a:

    $x(2x+34)=20 \times 1775 \Leftrightarrow 2x^2+34x-35500=0 \Leftrightarrow x^2+17x-17750=0 (\text
    E)$

    On obtient un trinôme du second degré, calculons son discriminant $\Delta$:

    $\Delta=17^2-4\times 1\times (-17750)=71289={267}^2$.

    $\Delta>0$ donc  $(\text E)$ admet deux racines distinctes.

    $x_1=\frac{-17-267} 2=-142$ et  $x_2=\frac{-17+267} 2=125$ 

    On garde la solution positive.

    La dimension d'un côté de la ville est  $2x_2=250$ $m$ .

