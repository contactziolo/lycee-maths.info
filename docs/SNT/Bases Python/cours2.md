---
date: September 5, 2022
geometry: "left=2cm,right=2cm,top=1.3cm,bottom=1.3cm"
output: pdf_document
---
# Structure Alternative

Elle permet d’exécuter, à un instant donné, une action ou une autre action, exclusivement l’une de l’autre. Il s’agit donc de 2 ou plusieurs actions qui se réalisent en fonction de certaines conditions.

## Syntaxe de la structure alternative 

On trouve deux syntaxes possibles : 

**L’alternative appauvrie :**	

```
SI « Expression logique » ALORS
	« Instruction si vrai »
FIN SI
```

**L’alternative complète :** 

```	
SI « Expression logique » ALORS
	« Instruction si vrai »
SINON
	« Instruction si faux »
FIN SI
```

*Exemple:* Calcul d’un montant TTC

Afin de calculer le Montant TTC d’une facture, il faut prendre en considération les éléments suivants :

- Si le net commercial est supérieur à 2000€ HT, vous est accordée une remise de 5% du net commercial.
- Dans tous les cas le taux de TVA est de 20%.

**Variables :**
```
NC	: Réel (Net commercial)
R	: Réel (Montant de la remise)
TTC	: Réel (Montant TTC)

Constante :

TVA ← 1.2
```
**Début**
```
Saisir « Net commercial ? » , NC

SI NC>2000 ALORS
    R ← NC x 5%
SINON
    R ← 0
FINSI

TTC ← (NC – R) * TVA
Afficher « Montant de la remise » , R
Afficher « Montant TTC : » , TTC
```
**Fin**

*Remarque*

Cet algorithme peut être simplifié, avec un structure alternative appauvrie, si on ne désire pas afficher la remise :

**Variables :**
```
NC	: Réel (Net commercial)
TTC	: Réel (Montant TTC)

Constante

TVA ← 1.2
```
**Début**
```
Saisir « Net commercial ? » , NC

SI NC>2000 ALORS
    NC ← NC * 0.95
FINSI

TTC ← NC x TVA
Afficher « Montant TTC : » , TTC
```
**Fin**

## La structure alternative imbriquée 

Il existe deux types de structures imbriquées :

### Le OU

Dans ce cas il faut que l’une des conditions soit vraie pour que l’instruction « vrai » se réalise.

*Exemple :* 

Cette fois la remise est de 5% si le net commercial est supérieur à 2000€ et de 2% si le net commercial est supérieur à 1000€.

**Variables :**
```
NC	: Réel (Net commercial)
R	: Réel (Montant de la remise)
TTC	: Réel (Montant TTC)

Constante

TVA ← 1.2
```
**Début**
```
Saisir « Net commercial ? » , NC

SI NC>2000 ALORS
    R← NC x 5%
SINON
	SI NC > 1000 ALORS
	    R ← NC x 2%
	SINON
	    R ← 0
	FINSI
FINSI

TTC ← (NC – R) * TVA
Afficher « Montant de la remise » , R
Afficher « Montant TTC : » , TTC
```
**Fin**

### Le ET

Dans ce cas il faut que toutes les conditions soient  réunies pour que l’instruction « vrai » se réalise.

*Exemple :* 

Cette fois la remise est de 5% si le net commercial est supérieur à 2000€ et si le délai de règlement prévu est inférieur à 30 jours

**Variables :**
```
NC	: Réel (Net commercial)
R	: Réel (Montant de la remise)
TTC	: Réel (Montant TTC)
DR	: Entier (Délai de règlement)

Constante

TVA ← 1.2
```
**Début**
```
Saisir « Net commercial ? » , NC
Saisir « Délai de règlement ? » , DR

SI NC>2000 ALORS
	SI DR < 30 ALORS
	    R ← NC x 5%
    SINON
        R ← 0
    FINSI
SINON
    R ← 0	
FINSI

TTC ← (NC – R) * TVA
Afficher « Montant de la remise » , R
Afficher « Montant TTC : » , TTC
```
**Fin**

## En plus

La structure alternative algorithmique ressemble à l’instruction conditionnelle d’Excel :

```
= SI(« Condition » ; « Action si condition vrai » ; « Action si condition fausse »)
```

Lorsque l’on réalise une structure alternative, il est préférable de la décaler grâce à une indentation (cf ci-dessus), cela permet de ne pas oublier les FINSI et de faciliter la lecture de l'algorithme. Certains utilisent aussi des accolades.

D'ailleurs en Python, ces `finsi` n'existent pas et la seule façon de déterminer la fin d'une `Action si condition` est justement l'indentation dans le code.