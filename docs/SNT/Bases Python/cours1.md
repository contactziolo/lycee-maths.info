---
date: September 5, 2022
geometry: "left=2cm,right=2cm,top=1.3cm,bottom=1.3cm"
output: pdf_document
---
# Variables et premières instructions

## Qu'est-ce qu'un algorithme ?

*Définition :*

L'algorithme est une décomposition en différentes étapes d'un problème à résoudre. Il s'agit d'un ensemble de règles opératoires propres à un calcul permettant d'arriver à un résultat déterminé.

*Contenu d'un algorithme :*

Un algorithme est composé d'opérations enchaînées suivant un ordre de succession. Il comporte éventuellement des conditions de séquences distinctes (structure alternative), ou plusieurs exécutions d'une même séquence (structure itérative).

*Objectif d'un algorithme :*

Réaliser des programmes informatiques.

*Représentation d'un algorithme :*

-Organigramme

**-Langage structuré.**

*Exemple d'algorithme :*

On désire calculer la vitesse en km/h d'un coureur sur une distance variable. On a besoin de connaître la distance (variable D) et le temps réalisé (variable T) par le coureur en seconde, afin de calculer et d'afficher sa vitesse en km/h (variable V).


**Variables**

D: Réel

T: Réel

V: Réel

**Début**

**Afficher** «Quelle est la distance parcouru en mètres ?»

Saisir D

Afficher «Quel est le temps réalisé en seconde?»

Saisir T 

$V \leftarrow {\frac{D \div 1000}{T \div (60 \times 60)}}$

**Afficher** «La vitesse en km/h est de:» , **V**

**Fin**


## Les variables

La clause «Variables», placée en début d'algorithme, permet de décrire toutes les informations nécessaires à la mise en oeuvre du traitement algorithmique. Chacune de ces informations (ou variables) est identifiée par un nom qui doit être le plus compréhensible possible.

Les types de valeurs pour une variable sont par exemple :

<center>

|Type| Correspondance|
|--------|--------|
|Réel|ensemble $\mathbb{R}$|
|Entier relatif|ensemble $\mathbb{Z}$|
|Chaîne de caractères|suite quelconque de caractères|
|Booléen|Vrai ; Faux|

</center>
## Les instructions élémentaires

### *L'instruction Afficher*

Syntaxe n°1 : Afficher «Texte»

L'exécution de cet ordre provoque l'affichage sur l'écran du texte spécifié entre guillemets.

Exemple : Afficher «Quelle est la distance parcourue en mètre ?»

A l'écran apparaîtra : Quelle est la distance parcourue en mètre ?

Syntaxe n°2 : Afficher Nom_variable

L'exécution de cet ordre provoque l'affichage du contenu de la variable dont le nom est mentionné.

Exemple : Afficher «La vitesse en km/h est de :» , **V**

A l'écran apparaîtra : La vitesse en km/h est de 36

(dans l'hypothèse ou coureur réalise un 100 mètres en 10 secondes)

### *L'instruction Entrer ou Saisir*

Syntaxe : Saisir Nom_variable

A l'exécution de cet ordre, l'algorithme demande à l'utilisateur de lui communiquer, par l'intermédiaire du clavier, une valeur.

Exemple : Saisir D

Il faut saisir la distance parcourue en mètre

### *L'instruction d'affectation : $\leftarrow$*

Syntaxe : Nom_variable $\leftarrow$ Expression

A l'exécution de cet ordre, l'ordinateur évalue l'expression. Le signe **$\leftarrow$** ressemble beaucoup au **=** en mathématique.

Exemple : $V \leftarrow {\frac{D \div 1000}{T \div (60 \times 60)}}$


### *En plus ...*

Les instructions *Saisir ou Entrer expriment le même besoin*, celui de communiquer une valeur à l'algorithme

On peut noter le texte et l'instruction de saisie sur la même ligne avec la syntaxe suivante :

**Saisir « Quelle est la distance parcouru en mètre ? » , D**

**Attention !**
Le signe **=** en algorithme correspond à une instruction de comparaison et non d'affectation, pour affecter on utilise bien $\leftarrow$
