---
date: September 5, 2022
geometry: "left=2cm,right=2cm,top=1.3cm,bottom=1.3cm"
output: pdf_document
---
#
## Exercice 1
```
DEBUT
    AFFICHER(1*9)
    AFFICHER(2*9)
    AFFICHER(3*9)
    AFFICHER(4*9)
    AFFICHER(5*9)
    AFFICHER(6*9)
    AFFICHER(7*9)
    AFFICHER(8*9)
    AFFICHER(9*9)
    AFFICHER(10*9)
FIN
```
Écrire un algorithme équivalent en utilisant la boucle POUR...FINPOUR

## Exercice 2
Julie place 430€ sur un livret bancaire au taux annuel de 3%.

Écrire un algorithme qui détermine et affiche le nombre d'années au bout desquelles Julie disposera d’une somme de 600€ sur ce livret bancaire.

## Exercice 3
Ecrire un algorithme qui affiche 50 fois "Je dois ranger mon bureau" à l'aide de l'instruction `for`.

## Exercice 4
1. Ecrire un algorithme qui permet de jouer au juste prix (il faut trouver un nombre entre 1 et 100).
2. Modifier le programme pour que le joueur soit limité à 5 essais !

## Exercice 5 Plus difficile

Ecrire un programme qui affiche un joli sapin de Noël, dont la taille est donnée par l’utilisateur. 

Exemple pour une taille de 12 lignes :

                  ^
                 ^^^
                ^^^^^
               ^^^^^^^
              ^^^^^^^^^
             ^^^^^^^^^^^
            ^^^^^^^^^^^^^
           ^^^^^^^^^^^^^^^
          ^^^^^^^^^^^^^^^^^
         ^^^^^^^^^^^^^^^^^^^
        ^^^^^^^^^^^^^^^^^^^^^
       ^^^^^^^^^^^^^^^^^^^^^^^