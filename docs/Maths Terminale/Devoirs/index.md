---
hide:
  -footer
---
#
**Devoirs pour les élèves de Terminale en spécialité Maths**

!!! attention
    - N'hésitez pas à bien chercher avant de regarder les corrigés.
    - La rédaction est très importante.

## Suites récurrence

[2022-2023 1h](ptds-suites-recurr-22-23.md) &nbsp;&nbsp; [2021-2022 0.33h](ptds-recur-21-22.md)   &nbsp;&nbsp; [2020-2021 0.5h](ptds-suites-recur-20-21.md)   &nbsp;&nbsp; [2020-2021 2h](ds-suites-recurr-20-21.md)  &nbsp;&nbsp;  [2019-2020 0.5h](ptds-suites-recur-19-20.md)  &nbsp;&nbsp;  [2018-2019 0.5h](ptds-recur-18-19.md)  &nbsp;&nbsp;  [2017-2018 1h](ds-suites-recurr-17-18.md)   &nbsp;&nbsp;  

## Probabilités - Loi binomiale

[2021-2022 1h](ds-loi-binomiale-21-22.md) &nbsp;&nbsp;  [2020-2021 0.5h](ds-loi-binomiale-20-21.md) &nbsp;&nbsp;  [2018-2019 1h](ds-loi-binomiale-18-19.md)

