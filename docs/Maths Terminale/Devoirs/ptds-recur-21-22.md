---
show:
  - navigation
hide:
  - toc
---
#
=== "`Sujet`"
    <center>
    **Petit devoir sur la Récurrence**
    </center>
    <center>
    *Un soin particulier sera apporté à la qualité de la rédaction.*
    </center>
    <center>
    *L'usage de la calculatrice est recommandé.*
    </center>
    <center>
    *Durée : 20 minutes*
    </center>
    Soit la suite numérique définie sur l'ensemble des entiers naturels ℕ
    par :

    <center>
    $u_0=2$ et $u_{n+1}=\frac 1 5u_n+3\times 0,5^n$ pour $n>0$
    </center>

    1.  Recopier et compléter ce tableau à l'aide de la calculatrice :
        On donnera des valeurs approchées à $10^{-4}$ des termes de la suites.

        |$n$|0|1|2|3|4|5|
        |-|-|-|-|-|-|-|
        |$u_{n}$|2| | | | | |

    2.  D'après ce tableau, énoncer une conjecture sur le sens de variation
        de la suite et sur le comportement de la suite à l'infini.

    3.  Démontrer par récurrence que pour tout entier non nul, on a:
        $u_n\geqslant \frac{15} 4\times 0,5^n$.

    4.  En déduire que pour tout entier naturel non nul, $u_{n+1}-u_n\leqslant 0$ .

    5.  Conclure.

=== "`Corrigé`"
    1. Tableau de valeurs

        |$n$|0|1|2|3|4|5|
        |-|-|-|-|-|-|-|
        |$u_{n}$|2|3.4|2.18|1.186|0.6122|0.3099|

    2. Avec le tableau et la calculatrice, on peut conjecturer que la suite est décroissante et converge vers $0$.

    3. Démonstration par récurrence.
        
        Soit l'hypothèse de récurrence: 

        $$\text{H}_{n} : «{u_{n} \geqslant \frac{15}{4} \times 0.5^n}»$$
        
        <u>*Initialisation*</u>

        $u_1=3.4$ et $\frac{15}{4} \times 0.5=1.875$ 
        
        la propriété est donc vraie pour $n=1$ .

        <u>*Hérédité*</u>

        Supposons $\text{H}_{n}$ vraie pour un rang $n$ donné, montrons qu'alors $\text{H}_{n+1}$ est vraie.

        On a $u_{n} \geqslant \frac{15}{4} \times 0.5^n$ ⇒ $\frac{1}{5} \times u_{n} \geqslant \frac{1}{5} \times \frac{15}{4} \times 0.5^n$

        ⇒ $\frac{1}{5}u_{n} +3\times 0,5^n\geqslant \frac{3}{4} \times 0.5^n+3\times 0,5^n$
        
        ⇒ $u_{n+1} \geqslant \frac{15}{4} \times 0.5^n$

        Or $1\geqslant \frac{1}{2}$ ⇒ $\frac{15}{4} \times 0.5^n \geqslant \frac{15}{4} \times 0.5^{n+1}$

        D'où  $u_{n+1} \geqslant \frac{15}{4} \times 0.5^{n+1}$ et l'hérédité est vraie.

        <u>*Conclusion*</u>

        La propriété $\text{H}_{n}$ est initialisée pour $n=1$ et héréditaire, par récurrence sur $n$ elle est donc vraie pour tout $n \in ℕ^*$ .

    4. On calcule $u_{n+1}-u_n=\frac 1 5u_n+3\times 0,5^n-u_n=-{\frac{4}{5}u_n}+3\times 0,5^n$

        Or, $u_n\geqslant \frac{15} 4 \times 0.5^n$ ⇒ $-{\frac{4}{5}u_n} \leqslant -3 \times 0.5^n$  

        ⇒ $-{\frac{4}{5}u_n} +3 \times 0.5^n \leqslant -3 \times 0.5^n +3 \times 0.5^n$ 

        ⇒ $-{\frac{4}{5}u_n} +3 \times 0.5^n \leqslant 0$

    5. La suite $(u_n)$ est décroissante.
