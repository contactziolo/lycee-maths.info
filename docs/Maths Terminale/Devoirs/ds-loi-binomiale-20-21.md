---
show:
    - navigation
hide:
    - toc
---
=== "`Sujet`"
    <center>

    # Petit Devoir sur la loi binomiale

    *Durée : 30 minutes.*

    *L’usage de la calculatrice est autorisé.*

    *Un soin particulier sera apporté à la rédaction et aux justifications.*
    </center>

    ## Exercice 1 
    Dans un club de sport, Jihane joue au basket. Elle sait que lors d’un lancer sa probabilité de marquer un panier est égale à $0.6$. 

    Jihane lance le ballon $n$ fois de suite.

    Les lancers sont indépendants les uns des autres, parce que Jihane a un moral en béton.

    Soit $\text{X}$ la variable aléatoire qui compte le nombre de paniers réussis.

    1. Quelle est la loi de probabilité de $\text{X}$ ? Bien justifier.
        
    2. Jihane tente 4 paniers consécutifs.

        **a)** Montrer que la probabilité que Jihane ne marque aucun panier est égale à $0.0256$.

        **b)** Calculer la probabilité que Jihane marque au moins deux paniers.
        
    3. Combien de fois Jihane doit-elle lancer le ballon au minimum pour que la probabilité qu’elle marque au moins un panier soit supérieure à $0.999$ ?

    ## Exercice 2 
    Indiquer en justifiant si les affirmations suivantes sont vraies ou fausses. 

    1. Si $\text{X}$ est une variable aléatoire suivant la loi $\text{B}(n,\frac{1}{3})$ avec $n \geqslant2$, alors $\text{P}(\text{X} \geqslant 1)=1-\left(\cfrac{2}{3}\right)^n$.

    2. Si $\text{X}$ suit la loi $\text{B}(5,p)$ avec $p \in ]0;1[$ et si $\text{P}(\text{X}=1)=\dfrac{5}{3}\text{P}(\text{X}=0)$ alors $\text{P}(\text{X}=2)=3\text{P}(\text{X}=3)$ .

    3. Si $\text{X}$ est une variable aléatoire qui suit une loi binomiale avec $\text{E}(\text{X})=36$  et $\sigma(\text{X})=3$ alors $\text{P}(\text{X}=29) \approx 0.01$ à $10^{-3}$ près.

=== "`Corrigé`"

    ## Exercice 1 
    1. L’expérience « lancer le ballon » a deux issues possibles : soit le panier est marqué, avec une probabilité de 0.6, soit il ne l’est pas, avec une probabilité de $1-0.6=0.4$.

    Cette expérience est répétée de façon identique et indépendante $n$ fois de suite.

    La variable aléatoire $\text{X}$ qui compte le nombre de paniers réussis suit donc une loi binomiale de paramètres $n$ et $p=0.6$.

    On a $p(\text{X}=k)=\left( \begin{array}{c} n \\ k \\ \end{array} \right) {0.6}^{k}{0.4}^{n-k}$.

    2. On prend $n=4$.

        **a)** $p(\text{X}=0)=\left( \begin{array}{c} 4 \\ 0 \\ \end{array} \right) {0.6}^{0}{0.4}^{4}=0.0256$
        
        **b)** $p(\text{X}\geqslant 0)=1-p(\text{X}= 0)-p(\text{X}= 1)$
        
        On calcule $p(\text{X}=1)=\left( \begin{array}{c} 4 \\ 1 \\ \end{array} \right) {0.6}^{1}{0.4}^{3}=0.1536$

        D’où $p(\text{X}\geqslant 0)=1-0.0256-0.1536=0.8208$

        La probabilité que Jihane marque plus de deux paniers est $0.8208$.

    3. $p(\text{X}\geqslant 1)=1-p(\text{X}=0)=1-\left( \begin{array}{c} n \\ 0 \\ \end{array} \right) {0.6}^{0}{0.4}^{n}=1-0.4^n$

        On cherche $n$ tel que $1-{0.4}^n \geqslant 0.999$ avec la calculatrice, et grâce à la décroissance de la suite $(u_n)$ telle que $u_n=0.4^n$, on trouve $n\geqslant 8$.

        À partir de 8 lancers, la probabilité que Jihane marque au moins un panier sera supérieure à $0.999$.

    ## Exercice 2
    1. $\text{X}$ suit la loi binomiale $\text{B} \left(n;\dfrac{1}{3} \right)$ avec $n \geqslant 2$.

        On a $p(\text{X}\geqslant 1)=1-p(\text{X}=0)=1-\left( \begin{array}{c} n \\ 0 \\ \end{array} \right) \left( \dfrac{1}{3} \right)^0\left( \dfrac{2}{3} \right)^n=1-\left( \dfrac{2}{3} \right)^n$.

        L’affirmation est donc vraie.

    2. $\text{X}$ suit la loi $\text{B} \left(5;p \right)$ donc:

        $p(\text{X}=0)=\left( \begin{array}{c} 5 \\ 0 \\ \end{array} \right) p^0(1-p)^5=(1-p)^5$

        $p(\text{X}=1)=\left( \begin{array}{c} 5 \\ 1 \\ \end{array} \right) p^1(1-p)^4=5p(1-p)^4$

        $p(\text{X}=2)=\left( \begin{array}{c} 5 \\ 2 \\ \end{array} \right) p^2(1-p)^3=10p^2(1-p)^3$

        $p(\text{X}=3)=\left( \begin{array}{c} 5 \\ 3 \\ \end{array} \right) p^3(1-p)^2=10p^3(1-p)^2$

        <u>
        Première méthode:
        </u>

        $p(\text{X}=1)=\dfrac{5}{3}p(\text{X}=0) \Leftrightarrow 5p(1-p)^4=\dfrac{5}{3}(1-p)^5 \Leftrightarrow p=\dfrac{1}{3}(1-p) \Leftrightarrow \dfrac{4}{3}p=\dfrac{1}{3} \Leftrightarrow p=\dfrac{1}{4}$

        On résout ensuite: $p(\text{X}=2)=3p(\text{X}=3) \Leftrightarrow 10p^2(1-p)^3=3 \times p^3(1-p)^2 \Leftrightarrow (1-p)=3p \Leftrightarrow p=\dfrac{1}{4}$

        <u>
        Deuxième méthode:
        </u>
        
        $p(\text{X}=1)=\dfrac{5}{3}p(\text{X}=0) \Leftrightarrow 5p(1-p)^4=\dfrac{5}{3}(1-p)^5 \Leftrightarrow 5p(1-p)^2=\dfrac{5}{3}(1-p)^3 \Leftrightarrow 3 \times 5p(1-p)^2=5(1-p)^3$ 
        
        $\Leftrightarrow 15p^3(1-p)^2=5p^2(1-p)^3 \Leftrightarrow 30p^3(1-p)^2=10p^2(1-p)^3 \Leftrightarrow 3 \times 10p^3(1-p)^2=10p^2(1-p)^3 \Leftrightarrow p(\text{X}=2)=3p(\text{X}=3)$

        L’affirmation est vraie.

    3. On a $\sigma(\text{X})=3 \Leftarrow \text{V}(\text{X})=9$ 
        
        $\text{X}$ suivant une loi binomiale, on a $\text{E}(\text{X})=36=np$ et $\text{V}(\text{X})=9=np(1-p)$
        
        D’où $9=36(1-p)$  et $p=\dfrac{3}{4}$. On déduit aussi $n \times \dfrac{3}{4}=36$ soit $n=48$.
        
        $\text{X}$ suit donc une loi binomiale $\text{B} \left(48;\dfrac{3}{4} \right)$ .
        
        Reste à calculer, avec la machine,  $p(\text{X}=29)=\left( \begin{array}{c} 48 \\ 29 \\ \end{array} \right) \dfrac{3}{4}^{29}\dfrac{1}{4}^{48-29} \approx 0.01$ à $10^{-3}$. 

        L’affirmation est (encore) vraie.