---
show:
  - navigation
hide:
  - toc
---
#
=== "`Sujet`"
    <center>
    *Durée: 25 minutes*
    </center>

    **Exercice 1**

    Soit $(u_n)$ la suite définie par : $u_0=0$ et pour tout entier naturel $n$ , $u_{n+1}=u_n+2n+2$ . 

    Montrer, à l’aide d’un raisonnement par récurrence, que le terme de rang $n$ de la suite $(u_n)$ admet pour expression : $u_n=n^2+n$ .

    **Exercice 2**

    On considère la suite $(u_n)$ définie pour tout $n$ de ℕ par la relation de récurrence:
    <center>
    $u_0=1$ et $u_{n+1}=1-\frac 1 4u_n-\frac 1 2u_n^2$
    </center>

    1. Construire les 5 premiers termes de  sur l'axe des abscisses du repère représenté au dos de cette feuille (la courbe représentative de la fonction $x$ $\mapsto$ $1-\frac 1 4x-\frac 1 2x^2$  est déjà tracée).

    2. Vérifier par récurrence que pour tout $n$ de ℕ, on a $0<u_n\leqslant 1$ .

      <img src="../img/courbe-18-19.png" width="60%" style="margin-left:15%;">

=== "`Corrigé`"
    **Exercice 1**

    Soit l'hypothèse de récurrence: $\text{H}_{n}$ : $«{u_{n} = n^2+n}»$.

    <u>*Initialisation*</u>

    $u_0=0$ et $0^2+0=0$ la propriété est donc vraie pour $n=0$ .

    <u>*Hérédité*</u>

    Supposons $\text{H}_{n}$ vraie pour un rang $n$ donné, montrons qu'alors $\text{H}_{n+1}$ est vraie.

    On a $u_{n+1}=u_n+2n+2$ ⇒ $u_{n+1}=n^2+n+2n+2=n^2+3n+2$

    Or $(n+1)^2+n+1=n^2+2n+1+n+1=n^2+3n+2$

    D'où  $u_{n+1}=(n+1)^2+n+1$ et l'hérédité est vraie.

    <u>*Conclusion*</u>

    La propriété $\text{H}_{n}$ est initialisée pour $n=0$ et héréditaire, par récurrence sur $n$ elle est donc vraie pour tout $n \in ℕ$ .

    **Exercice 2**

    1. Construction classique.

        On commence par tracer la droite d'équation $y=x$

    2. Démonstration par récurrence.

        <u>Travail préparatoire</u>: On commence par montrer que $f$ est décroissante sur $[0;1]$

        On a $f(x)=1-\frac 1 4x-\frac 1 2x^2$ une fonction du second degré.

        On calcule $-\frac b{2a}=\frac{-1} 4$ et on a $-\frac 1 2<0$ , $f$ est donc décroissante sur $\mathbb{R}^{+}$ .

        Soit l'hypothèse de récurrence: $\text{P}_{n}$ : $«0<u_n\leqslant 1»$.

        <u>*Initialisation*</u>

        On a $u_0=1$ et $0<1\leqslant 1$ donc $\text{P}_{0}$ est vérifiée.

        <u>*Hérédité*</u>

        Supposons $\text{P}_{n}$ vraie pour un rang $n$ donné, montrons qu'alors $\text{P}_{n+1}$ est vraie.

        On a $0<u_n\leqslant 1$ ⇒ $f(1) \leqslant f(u_n) < f(0)$ ( car $f$ décroissante)

        ⇒ $\frac 1 4 \leqslant u_{n+1} < 1$ ⇒ $0<u_{n+1} \leqslant 1$

        D'où $\text{P}_{n}$ ⇒ $\text{P}_{n+1}$

        <u>*Conclusion*</u>

        La propriété $\text{P}_{n}$ est initialisée pour $n=0$ et héréditaire, par récurrence sur $n$ elle est donc vraie pour tout $n \in ℕ$ .