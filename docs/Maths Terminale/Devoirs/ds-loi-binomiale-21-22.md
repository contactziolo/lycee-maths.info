---
show:
    - navigation
hide:
    - toc
---

=== "`Sujet`"

    <center>
    # Devoir de mathématiques

    *L’usage de la calculatrice est autorisé.*

    *Un soin particulier sera apporté à la rédaction.*

    *Durée : 1h*

    </center>

    ## Exercice 1 (3 points)
    On donne la loi de probabilité d’une variable aléatoire $\text{X}$ :

    |$x_i$|$-2$|$1$|$2$|$3$|$5$|
    |-|:-:|:-:|:-:|:-:|:-:|
    |$\text{P}(\text{X}=x_i)$|$\dfrac{1}{4}$|$\dfrac{1}{3}$|$\dfrac{1}{4}$|$\dfrac{1}{12}$|$\dfrac{1}{12}$|

    **a)** Déterminer la probabilité $\text{P}(\text{X}\geqslant 0)$.

    **b)** Calculer l’espérance de $\text{X}$. 

    **c)** Calculer l’écart-type de $\text{X}$.

    ## Exercice 2 (5 points)
    Un avion a une capacité de 100 personnes. On considère que la probabilité qu’une personne qui a réservé son billet ne se présente pas à l’embarquement est de 5%. 

    1. 100 billets, un par place, ont été vendus. 
        On note $\text{X}$ la variable aléatoire égale au nombre de personnes qui se présentent à l’embarquement.

        **a)** Préciser pourquoi $\text{X}$ suit une loi binomiale, et en donner les paramètres. 

        **b)** Calculer la probabilité que l’avion soit plein. 

        **c)** Calculer la probabilité pour qu’il reste au moins une place libre dans cet avion. 

        **d)** Calculer la probabilité qu’il y ait strictement moins de 96 personnes dans l’avion. 

    2. Comme on estime que la probabilité que cet avion ne soit pas plein est importante, on décide de vendre 105 billets pour ce vol.
        Calculer la probabilité qu’aucun client ne se retrouve sans place. 

    ## Exercice 3 (4 points)
    Un graphologue prétend être capable de déterminer le sexe d’une personne d’après son écriture dans 90% des cas.

    Personnellement, j’en doute. Pour préciser mon idée, je lui soumets 20 exemples d’écriture.
    Je suis prêt à reconnaître sa compétence s’il réussit au moins 90% des identifications du sexe, soit au moins 18 sur les 20.

    1. Quelle est la probabilité que je reconnaisse la compétence du graphologue alors qu’il s’est prononcé 20 fois complètement au hasard ?

    2. Quelle est la probabilité que je rejette l’affirmation du graphologue alors qu’elle est totalement fondée ? 

    3. Quel inconvénient présente ce test ? 

    ## Exercice 4 (2 points)
    Quelles sont les valeurs affichées par le programme Python suivant ? 
    ```python
    u=1
    v=1
    for i in range(5) : 
        w=u+v 
        v=u 
        u=w 
        print(i,w) 
    print(u/v) 
    ```

=== "`Corrigé`"

    ## Exercice 1 (3 points)

    **a)** $\text{P}(\text{X}\geqslant 0)=1-\text{P}(\text{X}<0)=1-\dfrac{1}{4}=\dfrac{3}{4}$

    **b)** $\text{E}(\text{X})=(-2) \times \dfrac{1}{4} + \dfrac{1}{3} + \dfrac{2}{4}+\dfrac{1}{4}+\dfrac{5}{12}=1$

    **c)** $\sigma(\text{X})=\sqrt{\text{E}(\text{X}^2)-(\text{E}(\text{X}))^2}\approx 2.04$ à $10^{-2}$

    ## Exercice 2 
    1. **a)** On répète 100 fois de façon identique et indépendante la même expérience: soit le passager se présente ($p=0.95$), soit il ne se présente pas ($\bar{p}=0.05$). On a donc une expérience à deux issues possibles répétée 100 fois et $\text{X}$ suit bien une loi binomiale de paramètres $n=100$ et $p=0.95$.

        **b)** $\text{P}(\text{X}=100)=0.95^100\approx0.006$ à $10^{-3}$ près.

        **c)** $\text{P}(\text{X}\<100)=1-\text{P}(\text{X}=100)\approx 0.994$ à $10^{-3}$ près.

        **d)** $\text{P}(\text{X}\<96)=\text{P}(\text{X} \leqslant 95)\approx 0.564$ à $10^{-3}$ près. (avec `binomFrèp')

    2. De la même façon que précédemment, il s'agit d'une loi binomiale de paramètres $n=105$ et $p=0.95$. On veut cette fois calculer $\text{P}(\text{X}\leqslant 100) \approx 0.608$ à $10^{-3}$ (Toujours avec `binomFrèp')

    ## Exercice 3
    1. Supposons que le graphologue réponde complètement au hasard et indépendamment pour chacune des 20 écritures. La probabilité de bonne réponse du graphologue, c'est-à-dire d’une bonne identification du sexe de la personne, est $p =\dfrac{1}{2}$.

        En notant $\text{X}$ la varaible aléatoire égale au nombre de bonnes réponses du graphologue sur les 20 identifications, on a donc que X suit la loi binomiale de paramètres $n=20$ et $p =\dfrac{1}{2}$.

        Je reconnais alors sa compétence avec une probabilité de $\text{P}(\text{X}\geqslant 18)$.

        Avec la calculatrice, on peut soit calculer $\text{P}(\text{X}\geqslant 18)=\text{P}(\text{X}=18)+\text{P}(\text{X}=19)+\text{P}(\text{X}=20)=1-\text{P}(\text{X}\leqslant 17)\approx 2\times10^{-4}$

        Avec ce test, la probabilité que je reconnaisse la compétence de quelqu'un qui répond au hasard est très faible.

    2. Si le graphologue a bien la compétence qu'il annonce, il identifie correctement le sexe avec une
    probabilité $p=0.9$.
        En notant cette fois $\text{Y}$ la variable aléatoire égale au nombre de bonnes identifications, $\text{Y}$ suit la loi binomiale de paramètres $n=20$ et $p = 0.9$ et la probabilité que je rejette son affirmation est $\text{P}(\text{Y} \leqslant 17) \approx 0.32$.

        Il y a donc environ une chance sur trois (environ 33%) pour que je me trompe !
        
        Avec ce test j'ai très peu de chance de me tromper face à un charlatan. Par contre, j'ai beaucoup de chances de considérer comme charlatan un graphologue qui sait réellement reconnaître le sexe d'une personne d’après son écriture (et qui sait se tromper dans 10% des cas).

    ## Exercice 4
    Le programme affiche successivement:

        0 2
        1 3
        2 5
        3 8
        4 13 
        1.625

    Il s'agit des premiers termes de la suite de Fibonacci. Le dernier nombre affiché est le rapport des deux derniers termes consécutifs. On peut montrer que ce rapport tend vers le nombre d'or, lorsque le nombre d’itérations tend vers l'infini. 
