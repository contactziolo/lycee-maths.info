---
show:
    - navigation
hide:
    - toc
---
#
=== "`Sujet`"
    <center>
    
    **Devoir n°1**

    **Suites et Récurrence**

    *L'usage de la calculatrice n'est pas autorisé.*

    *Le barème est donné à titre indicatif et n'a rien de définitif.*

    *Un soin particulier sera apporté à la rédaction.*

    *Durée: 60 minutes*

    </center>

    **Exercice 1 :** (4 points)

    La suite est définie par $u_0=2$ et pour tout entier naturel $n$ , $u_{n+1}=2u_n-3$

    1.  Représenter graphiquement les 5 premiers termes de cette suite au recto de cette feuille en utilisant une habile construction.

    2.  Démontrer par récurrence que pour tout entier naturel $n$ , $n\geqslant 0$ ,  $u_n=3-2^n$ .

    **Exercice 2 : **(2 points)

    Démontrer par récurrence que pour tout entier naturel $n\geqslant 4$ , $2^n>3n+1$ .

    **Exercice 3 :** (8 points)

    La suite est définie par $u_0=0$ et pour tout entier naturel $n$ , $u_{n+1}=3u_n-2n+3$ .

    1.  **a.** Calculer les valeurs exactes de $u_{1}$, $u_{2}$ et $u_{3}$.

        **b.** Démontrer par récurrence que pour tout entier $n$,
        $u_{n} \geqslant n$ .

        **c.** Utiliser ce dernier résultat pour montrer que $(u_{n})$ est
        croissante.

    2.  Pour tout entier naturel $n$, on pose $v_{n} = {{u_{n} - n} + 1}$

        **a.** Démontrer que $(v_{n})$ est une suite géométrique dont on
        donnera le premier terme et la raison.

        **b.** En déduire l'expression de $v_{n}$ puis de $u_{n}$ en
        fonction de $n$.

=== "`Corrigé`"
    **Exercice 1**

    1. Représentation graphique

        <img src="../img/graph17-18.png" width="60%" style="margin-left:10%;">

    2. Posons l'hypothèse de récurrence: $\text{P}_{n}$ :
        $«{u_{n} = {3 - 2^{n}}}»$.

        <u>*Initialisation:*</u>

        $u_{0} = 2$ et pour $n = 0$, ${3 - 2^{0}} = 2$ d'où
        $u_{0} = {3 - 2^{0}}$.

        <u>*Hérédité:*</u>

        Supposons que $\text{P}_{n}$ soit vraie pour un rang $n$ donné,
        montrons qu'alors $\text{P}_{n + 1}$ est vraie.

        $u_{n} = {3 - 2^{n}}$ ⇒ $u_{n+1}=2(3-2^n)-3$

        ⇒ $u_{n+1}=6-2^{n+1}-3$

        ⇒ $u_{n+1}=3-2^{n+1}$

        ⇒ $\text{P}_{n + 1}$ est vraie.

        <u>*Conclusion:*</u>

        Par récurrence sur $n$ , on a donc $u_n=3-2^n$ pour tout $n\in \mathbb{N}$ .

    **Exercice 2**

    Soit  $\text H_n$ l'hypothèse de récurrence:  $\text{«}2^n>3n+1\text{»}$

    <u>*Initialisation:*</u>

    Pour  $n=4$  on a  $2^4=16$ et  $3\times 4+1=13$ or  $16>13$ d'où  $\text H_4$  vraie.

    <u>*Hérédité:*</u>

    Supposons que  $\text H_n$ soit vraie pour un rang  $n$ donné, montrons qu'alors  $\text H_{n+1}$ est vraie.
    $2^n>3n+1$ ⇒  $2\times 2^n>2\times 3n+2$ ⇒  $2^{n+1}>6n+2$  $(\text I)$

    Comparons ensuite  $6n+2$ et $3n+4$ :

    $6n+2-(3n+4)=3n-2$ or  $3n-2\geqslant 0$ pour  $n\geqslant 1$ et à fortiori pour  $n\geqslant 4$ .
    D'où $6{{n + 2} > 3}{n + 5}$

    d'après $(\text{I})$ on a donc
    ${2^{n + 1} > 6}{{n + 2} > 3}{n + 4}$ avec
    $3{{n + 4} = 3}{{({n + 1})} + 1}$.

    et ${2^{n + 1} > 3}{{({n + 1})} + 1}$ ce qui signifie que
    $\text{H}_{n + 1}$ est vraie.

    <u>*Conclusion:*</u>

    Par récurrence sur $n$, on a donc ${2^{n} > 3}{n + 1}$ pour tout
    entier naturel $n \geqslant 4$.

    **Exercice 3**

    1.  **a.** ${u_{1} = 3}{{{u_0 - {2 \times 0}} + 3} = 3}$;
        ${u_{2} = 3}{{{u_{1} - {2 \times 1}} + 3} = 10}$;
        ${u_{3} = 3}{{{u_{2} - {2 \times 2}} + 3} = 29}$

        **b.** Soit $\text{Q}_{n}$ l'hypothèse de récurrence:
        $«{u_{n} \geqslant n}»$.

        <u>*Initialisation:*</u>

        On a $u_{0} = 0$ donc $\text{Q}_{0}$ est clairement vraie.

        <u>*Hérédité:*</u>

        Supposons que  $\text Q_n$ soit vraie pour un rang $n$ donné, montrons qu'alors  $\text Q_{n+1}$ est vraie.

        $u_n\geqslant n$ ⇒  $3u_n\geqslant 3n$ ⇒  $3u_n-2n+3\geqslant 3n-2n+3$
        ⇒  $u_{n+1}\geqslant n+3\geqslant n+1$ ⇒  $\text Q_{n+1}$ vraie.

        <u>*Conclusion:*</u>

        Par récurrence sur  $n$ , $\text Q_n$ est vraie pour tout $n$ entier naturel.

        **c.** $u_{n+1}-u_n=3u_n-2n+3-u_n=2u_n-2n+3$ . On a  $u_n\geqslant n$ ⇒  $2u_n\geqslant 2n$ ⇒  $2u_n-2n\geqslant 0$ .
        d'où  $2u_n-2n+3\geqslant 0$ et $u_{n+1}-u_n\geqslant 0$.  $(u_n)$ est donc croissante.

    2.  **a.** $v_{n+1}=u_{n+1}-(n+1)+1$ ⇒ $v_{n+1}=3u_n-2n+3-n-1+1$ ⇒  $v_{n+1}=3u_n-3n+3$ 
        d'où  $v_{n+1}=3(u_n-n+1)=3v_n$ d'où $(v_n)$ géométrique de raison 3 et de premier terme  $v_0=u_0-0+1=1$ .

        **b.** On a donc $v_n=3^n$ et $u_n=v_n+n-1=3^n+n-1$.