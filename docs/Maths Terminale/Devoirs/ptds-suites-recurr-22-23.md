---
hide:
  - toc
date: September 5, 2022
geometry: "left=2cm,right=2cm,top=1.3cm,bottom=1.3cm"
output: pdf_document
---

=== "`Sujet`"

    <center>
    # Petit Devoir 1 : Récurrence

    *L'usage de la calculatrice, bien que totalement inutile, est autorisé.*

    *Un soin particulier sera apporté à la rédaction.*

    *Durée : 38 min*
    </center>

    ## Exercice 1

    Dans cet exercice, $q$ est un nombre réel différent de 1.

    On note:

    $$\sigma_n=1+q+q^2+\cdots+q^n$$

    Par récurrence sur $n$, démontrer que:

    $$\sigma_n=\frac{1-q^{n+1}}{1-q}$$


    ## Exercice 2

    Montrer que pour tout entier naturel $n$, $n^3-n$ est un multiple de $3$.


    ## Exercice 3

    On définit une suite par $u_{n+1}=\sqrt{u_n+15}$ avec $n \in \mathbb{N}$.

    Montrer que si $u_0 \in[0;4]$ alors pour tout $n$ : $u_n \in [0;5]$, et que si $u_0 \in[5;10]$ alors pour tout $n$ : $u_n \in [4;10]$

=== "`Corrigé`"

    ## Exercice 1

    Soit $\text P(n)$  :  $\text{«}\sigma_n=\frac{1-q^{n+1}}{1-q}\text{»}$

    <u>*Initialisation*</u>

    On a $\sigma_0=q^0=1$ et $\frac{1-q^{1}}{1-q}=1$

    D'où $\text P(0)$ est vérifiée.

    <u>*Hérédité*</u>

    Supposons $\text P(n)$ vraie pour un rang n donné et montrons qu’alors $\text P(n+1)$ est vraie.

    $\text P(n) \Rightarrow 1+q+q^2+\cdots+q^n=\frac{1-q^{n+1}}{1-q} \Rightarrow$

    $\text P(n) \Rightarrow 1+q+q^2+\cdots+q^n+q^{n+1}=\frac{1-q^{n+1}}{1-q}+q^{n+1}$

    $\text P(n) \Rightarrow \sigma_{n+1}=\frac{1-q^{n+1}}{1-q}+\frac{(1-q)q^{n+1}}{1-q}$
    $\text P(n) \Rightarrow \sigma_{n+1}=\frac{1-q^{n+1}+q^{n+1}-q^{n+2}}{1-q}$

    $\text P(n) \Rightarrow \sigma_{n+1}=\frac{1-q^{n+2}}{1-q}$

    $\text P(n) \Rightarrow \text P(n+1)$

    <u>*Conclusion*</u>

    $\text P(0)$ est vérifiée et P est héréditaire. Par récurrence sur $n$ , on a donc $\sigma_n=\frac{1-q^{n+1}}{1-q}$ pour tout $n \in \mathbb{N}$.

    ## Exercice 2

    Soit $\text P(n)$  :  $\text{«}n^3-n \text{ est un multiple de } 3\text{»}$

    <u>*Initialisation*</u>

    Pour $n=0$, on a $n^3-n=0$ or $0=3 \times 0$

    $\text P(0)$ est bien vraie.

    <u>*Hérédité*</u>

    Supposons $\text P(n)$ vraie pour un rang $n$ donné et montrons qu’alors $\text P(n+1)$ est vraie.

    Si $n^3-n$ est un multiple de $3$, alors il existe $k \in \mathbb{Z}$ tel que $n^3-n=3k$.

    On calcule $(n+1)^3-(n+1)=n^3+3n^2+3n+1-(n+1)=n^3-n+3n^2+3n+1-1=3k+3n^2+3n=3(k+n^2+n)$

    D'où $(n+1)^3-(n+1)=3k'$ avec $k' \in \mathbb{Z}$

    <u>*Conclusion*</u>

    $\text P(0)$ est vérifiée et P est héréditaire. Par récurrence sur $n$ , on a donc $n^3-n \text{ est un multiple de } 3$ pour tout $n \in \mathbb{N}$.

    ## Exercice 3

    Pour $u_0\in[0;4]$:

    Soit $\text P(n)$ : $\text{«}u_n \in [0;5]\text{»}$

    <u>*Initialisation*</u>

    $u_0\in[0;4] \Rightarrow u_0\in[0;5] \Rightarrow \text P(0)$ vérifiée

    <u>*Hérédité*</u>

    Supposons $\text P(n)$ vraie pour un rang $n$ donné et montrons qu’alors $\text P(n+1)$ est vraie.

    $\text P(n) \Rightarrow 0 \leqslant u_n \leqslant 5$
    $\Rightarrow 15 \leqslant u_n+15 \leqslant 20$
    $\Rightarrow \sqrt{15} \leqslant \sqrt{u_n+15} \leqslant \sqrt{20}$ $(x\mapsto\sqrt{x} \nearrow)$

    or $\sqrt{15}>0$ et $\sqrt{20}<5$

    D'où $0 \leqslant u_{n+1} \leqslant 5 \Rightarrow \text P(n+1)$ est vérifiée.

    <u>*Conclusion*</u>

    $\text P(0)$ est vérifiée et P est héréditaire. Par récurrence sur $n$ , on a donc $u_n \in [0;5]$ pour tout $n \in \mathbb{N}$.

    Pour $u_0\in[5;10]$:

    Soit $\text P(n)$ : $\text{«}u_n \in [4;10]\text{»}$

    <u>*Initialisation*</u>

    $u_0\in[5;10] \Rightarrow u_0\in[4;10] \Rightarrow \text P(0)$ vérifiée

    <u>*Hérédité*</u>

    Supposons $\text P(n)$ vraie pour un rang $n$ donné et montrons qu’alors $\text P(n+1)$ est vraie.

    $\text P(n) \Rightarrow 4 \leqslant u_n \leqslant 10$
    $\Rightarrow 19 \leqslant u_n+15 \leqslant 25$
    $\Rightarrow \sqrt{19} \leqslant \sqrt{u_n+15} \leqslant \sqrt{25}$ $(x\mapsto\sqrt{x} \nearrow)$ 

    or $\sqrt{19}>4$ et $\sqrt{25}<10$

    D'où $4 \leqslant u_{n+1} \leqslant 10 \Rightarrow \text P(n+1)$ est vérifiée.

    <u>*Conclusion*</u>

    $\text P(0)$ est vérifiée et P est héréditaire. Par récurrence sur $n$ , on a donc $u_n \in [4;10]$ pour tout $n \in \mathbb{N}$.