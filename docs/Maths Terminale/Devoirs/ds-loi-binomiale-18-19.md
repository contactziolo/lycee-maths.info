---
show:
    - navigation
hide:
    - toc
---
=== "`Sujet`"
    <center>
    # Devoir Probabilités et Suites

    *Un soin particulier sera apporté à la rédaction et à la précision des justifications*

    *L’usage de la calculatrice est autorisé.*

    *Durée : 55 min*

    </center>

    En 2018, la municipalité d’un village a mis en place le service vélo en liberté. Il s'agit d'un service de location de vélos à la journée.

    Les vélos sont disponibles sur deux sites $\text{A}$ et $\text{B}$ et doivent être ramenés en fin de journée indifféremment sur l'un des deux sites.

    - si un vélo est loué sur le site A, la probabilité d'être ramené en $\text{A}$ est 0,7 ;
    - si un vélo est loué sur le site B, la probabilité d'être ramené en $\text{B}$ est 0,8 ;
    - Le premier jour, tous les vélos sont distribués également sur les deux sites.

    Les résultats numériques seront arrondis à $10^{−3}$ près.

    ## Partie A : Les deux premiers jours ...
    On choisit un vélo au hasard et on considère les événements : 

    $\text{E}_1$ : « le vélo est situé sur le site $\text{A}$ la première journée » ; 

    $\text{E}_2$ : « le vélo est situé sur le site $\text{A}$ la deuxième journée ». 

    1. Recopier et compléter l'arbre des probabilités ci-dessous.

        <img src="../img/arbr1.png" width="30%" style="margin-left:15%;">

    2. En déduire la probabilité pour le vélo soit ramené sur le site $\text{A}$ la seconde journée.

    3. On constate que le vélo a été ramené sur le site $\text{A}$ la seconde journée.
        Quelle est la probabilité qu'il se soit trouvé sur le site $\text{B}$ la veille ?

    ## Partie B : Les jours suivants ...

    Pour tout entier $n \geqslant1$ , on note $p_n$ la probabilité que le vélo soit situé sur le site $\text{A}$ la n -ième journée.

    1. Établir que, pour tout entier $n \geqslant 1$, on a $p_{n+1}=0.5p_n+0.2$

        On pourra s'aider de l'arbre pondéré ci-dessous.

        <img src="../img/arbr2.png" width="30%" style="margin-left:15%;">

    2. On note $(u_n)_{n \in \mathbb{N}}$ la suite définie par $u_n=p_n-0.4$ .

        - Montrer que la suite $(u_n)_{n \in \mathbb{N}}$ est une suite géométrique.

        - Préciser ses éléments caractéristiques.

    3. Donner l'expression de $u_n$, puis celle de $p_n$ en fonction de $n$

    4. En déduire la valeur de $\lim\limits_{n \to \infty}p_n$ . Interpréter le résultat. 

    ## Partie C : Et si on répétait ?
    Après bien des années de mise en place de ce service vélo en liberté, on supposera que $p_n=0.4$. 

    Le parc vélocipède comprend 10 vélos.

    On supposera que les vélos sont empruntés par des personnes qui ne se connaissent pas et ne se sont pas rencontrées. On supposera, aussi, que le trajet suivi pendant la journée de location a été largement aléatoire.

    Soit $\text{X}$ le nombre de vélos situés sur le site $\text{A}$ le 18 octobre 2137.

    1. Justifier Soigneusement la nature de la loi de probabilité suivie par X. Déterminer la probabilité qu’au moins un vélo soit en $\text{A}$ à la fin de cette journée.

    2. Déterminer la probabilité qu’au moins un vélo soit en $\text{A}$ à la fin d’un des trois premiers jours de décembre 2137.

=== "`Corrigé`"

    ## Partie A
    1. L'arbre complet est donné ci-dessous.

        <img src="../img/arbre1-18-19.png" width="30%" style="margin-left:15%;">


    2.  $\text{E}_1$ et $\overline{\text{E}_1}$ réalisent une partition de $\text{E}_1$, avec la formule des probabilités totales :
        
        $$p(\text{E}_2)=p(\text{E}_2 \cap \text{E}_1)+p(\text{E}_2 \cap \overline{\text{E}_1})=0.7 \times 0.5+0.2 \times 0.5=0.45$$
        
        La probabilité pour le vélo soit ramené sur le site $\text{A}$ la seconde journée est $0.45$

    3. La probabilité demandée est $p_{\text{E}_2}(\overline{\text{E}_1})$

        $p_{\text{E}_2}(\overline{\text{E}_1})=\dfrac{p(\text{E}_2 \cap \overline{\text{E}_1})}{p(\text{E}_2)}=\dfrac{0.2 \times 0.5}{0.45}=\dfrac{0.1}{0.45}=\dfrac{2}{9} \approx 0.222$

        Donc, la probabilité que le vélo se soit trouvé sur le site $\text{B}$ la veille est $0.222$ à $10^{-3}$.

    ## Partie B
    On note $\text{E}_n$ l'événement : « le vélo est situé sur le site $\text{A}$ la $n^{\text{ième}}$ journée »;
    Ainsi, $p_n=p(\text{E}_n)$ et on peut représenter la situation relative à deux jours consécutifs grâce à l'arbre pondéré ci-dessous.

    <img src="../img/arbre1-18-19.png" width="30%" style="margin-left:15%;">

    1. Partitions+probas totales. 
        
        Pour tout entier $n \geqslant 1$, on a: $p_{n+1}=p(\text{E}_{n+1})=p(\text{E}_{n+1} \cap \text{E}_{n})+p(\text{E}_{n+1} \cap \overline{\text{E}_{n}})$
        
        donc $p_{n+1}=0.7 \times p_{n}+0.2 \times (1-p_{n})=0.5p_{n}+0.2$


    2. Pour tout entier $n \geqslant 1$:

        $u_{n+1}=p_{n+1}-0.4=0.5p_n+0.2-0.4=0.5p_n-0.2=0.5(p_n-0.4)=0.5u_n$

        On en conclut que la suite $(u_n)$ est géométrique de raison $q=0.5$ et de terme initial $u_1=p_1-0.4=0.5-0.4=0.1$.

    3. Pour tout entier $n \geqslant 1$, $u_n=u_1\times0.5^{n-1}=\dfrac{0.1}{0.5}{0.5}^n=0.2 \times {0.5}^n$  .
        
        Comme $p_n=u_n+0.4$, on en déduit que $p_n=0.2\times{0.5}^n+0.4$.

    4. On a $-1<0.5<1$, d'où $\lim\limits_{n \rightarrow +\infty} {0.5}^n=0$, donc $\lim\limits_{n \rightarrow +\infty} p_n=0.4$.
        
        Au bout d'un grand nombre de jours, la proportion de vélo sur le site $\text{A}$ sera très proche de 40 % .

    ## Partie C
    1. On répète 10 fois l’expérience suivante de façon identique et indépendante :
        
        + Le vélo se trouve en $\text{A}$ : $p=0.4$
        
        + Le vélo ne se trouve pas en $\text{A}$ : $1-p=0.6$ 
        
        La variable aléatoire suit donc une loi binomiale de paramètres $n=10$ et $p=0.4$.
        
        On cherche à calculer $p(\text{X} \geqslant 1)=1-p(\text{X}=0)=1-0.6^{10} \approx 0.994$.

    2. On répète trois fois l’expérience suivante de façon identique et indépendante :
        
        + Au moins un vélo est en $\text{A}$ : $p=0.994$
        
        + Aucun vélo n’est en $\text{A}$ : $1-p=0.006$

        La variable aléatoire $\text{Y}$ qui compte le nombre de jours durant lesquels au moins un vélo est en $\text{A}$ lors des trois premiers jours de décembre suit donc une loi binomiale de paramètres $n=3$ et $p=0.994$.
        
        D’où $p(\text{Y} \geqslant 1)=1-p(\text{X}=0)=1-0.006^3 \approx 1$.